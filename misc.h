// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#include <fstream>
#include <utility>
#include <limits>
#include <cstdint>
#include <cstring>
#include <algorithm>

/// Swaps the byte order between big and little endian
template <typename T>
inline void endian_swap(T& m)
{
#if defined(__GNUC__) || defined(__GNUG__)
	if (sizeof(T) == 4) m = __builtin_bswap32(m);
	else
	{
		unsigned char *memp = reinterpret_cast<unsigned char*>(&m);
		std::reverse(memp, memp + sizeof(T));
	}
#elif defined(_MSC_VER) // not tested
	if (sizeof(T) == 4) m = _byteswap_ulong(m);
	else
	{
		unsigned char *memp = reinterpret_cast<unsigned char*>(&m);
		std::reverse(memp, memp + sizeof(T));
	}
#else
	unsigned char *memp = reinterpret_cast<unsigned char*>(&m);
	std::reverse(memp, memp + sizeof(T));
#endif
}

/// Determine the size (pixels width x height) of a png file. Returns 0x0 on errors.
std::pair<unsigned int, unsigned int> pngsize(const char* pngfile)
{
	std::ifstream png(pngfile, std::ios::binary);
	
	// http://www.libpng.org/pub/png/spec/1.0/PNG-Structure.html#Chunk-layout
	// http://www.libpng.org/pub/png/spec/1.0/PNG-Chunks.html#C.IHDR
	// note: integers must be in network byte order (big endian)
	const char magic_png[8] = { '\x89', 'P', 'N', 'G', '\x0D', '\x0A', '\x1A', '\x0A' };
	
	// the data can be found exactly in this order in the file:
	char magic_file[8];
	uint32_t chunklen;
	char chunktype[4]; // "IHDR" (Image header, always first)
	uint32_t width;
	uint32_t height;
	
	std::pair<unsigned int, unsigned int> result;
	result.first = 0;
	result.second = 0;
	
	if (!png.read(reinterpret_cast<char*>(&magic_file), 8)) return result;
	if (std::strncmp(magic_file, magic_png, 8)) return result;
	if (!png.read(reinterpret_cast<char*>(&chunklen), 4)) return result;
	if (chunklen < 8) return result;
	if (!png.read(reinterpret_cast<char*>(&chunktype), 4)) return result;
	if (std::strncmp(chunktype, "IHDR", 4)) return result;
	if (!png.read(reinterpret_cast<char*>(&width), 4)) return result;
	if (!png.read(reinterpret_cast<char*>(&height), 4)) return result;

#if __BYTE_ORDER__ == __ORDER_LITTLE_ENDIAN__ // on x86/x64
	endian_swap(width);
	endian_swap(height);
#endif

	if (width > std::numeric_limits<unsigned int>::max() || height > std::numeric_limits<unsigned int>::max()) return result;

	result.first = static_cast<unsigned int>(width);
	result.second = static_cast<unsigned int>(height);
	return result;
}

const unsigned int one_cm_is=360000; // use this unit for all calculations - here emu

unsigned int guess_scaling(unsigned int px_width, unsigned int px_height, unsigned int canvas_width, unsigned int canvas_height)
{
	// the result is a factor, to convert pixel to the desired emu size
	// 1 inch = 2,54 cm = 25,4 mm
	// typical word display magnification: 114 %
	// typical word canvas (A4, without the margins): 15x23 cm
	
	// the following resolutions are in pixel/mm, but *10 to avoid floating point arithmetic
	const unsigned int res_max = 70; // otherwise text may not be readable anymore - 82 seems to be the real maximum where the document already has to be magnified
	const unsigned int res_best = 60;
	const unsigned int res_one = 43; // resolution, where one pixel in a graphic in the document has same size as a screen pixel, when the document is shown typically magnified
	// const unsigned int res_min = 36; // don't expand graphics more than this resolution, except they have icon size. Typically about 35 and 38
	
	// pixel: If larger as any of the two values, graphics are not treated as icons. For icons the res_one resolution is preferred
	const unsigned int icon_sz_x = 200;
	const unsigned int icon_sz_y = 200;
	
	// minimum size, graphics are enlarged when smaller
	unsigned int min_units_x = 2 * one_cm_is; // 2 cm
	unsigned int min_units_y = 1 * one_cm_is; // 1 cm
	
	// do not enlarge the graphic more than this size, if res_max is not exceeded
	const unsigned int max_vspan = canvas_height / 100 * 70; // 70% of canvas height
	
	// ######################################
	
	unsigned int result;
	
	if (min_units_x > canvas_width) min_units_x = 0;
	if (min_units_y > canvas_height) min_units_y = 0;
	
	// note that because of the calculation the max value becomes a min value now
	const unsigned int val_min = one_cm_is/res_max;
	// const unsigned int val_max = one_cm_is/res_min;
	
	// const bool is_gfx_landscape = px_width > px_height;
	// const bool is_canvas_landscape = canvas_width > canvas_height;
	const bool is_iconsize = px_width <= icon_sz_x && px_height <= icon_sz_y;
	
	// start with defaults
	if (is_iconsize) result = one_cm_is/res_one;
	else result = one_cm_is/res_best;
	
	// fit to thesholds:
	
	// the screenshot should not be larger than this size (threshold: max_vspan converted to max_vspan_height_factor)
	// except it would not be readable anymore (threshold: res_max converted to val_min)
	const unsigned int max_vspan_height_factor = max_vspan/px_width;
	if (max_vspan_height_factor < result && max_vspan_height_factor > val_min) result = max_vspan_height_factor;
	
	// but it should be least have this minimum size, independent of being an icon or similar
	const unsigned int min_width_factor = min_units_x/px_width;
	if (min_width_factor > result) result = min_width_factor;
	const unsigned int min_height_factor = min_units_y/px_width;
	if (min_height_factor > result) result = min_height_factor;
	
	// and at last not larger than the page canvas
	const unsigned int max_canvas_width_factor = canvas_width/px_width;
	if (max_canvas_width_factor < result) result = max_canvas_width_factor;
	const unsigned int max_canvas_height_factor = canvas_height/px_height;
	if (max_canvas_height_factor < result) result = max_canvas_height_factor;

	return result;
}
