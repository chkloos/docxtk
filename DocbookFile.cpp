// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2017
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#include <fstream>

#include "DocbookFile.h"


bool DocbookFile::open(const std::string& path)
{
	this->path.clear();
	std::ifstream io;
	io.open(path);
	if (! io.is_open()) return false;
	xml_document.load(io, pugi::parse_default | pugi::parse_pi);
	io.close();
	if (!xml_document) return false;
	this->path = path;
	return true;
}
