// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#include <cstring>
#include <cstdio> // sscanf
#include <string>
#include <algorithm>
#include <cassert>
#include <functional>
#include <pugixml.hpp>
#include "Docbook2Word.h"
#include "XMLHelper.h"
#include "misc.h"
#include <iostream>


pugi::xml_node Docbook2Word::translate(const DocbookFile& from)
{
	current_db = &from;
	const pugi::xml_document& docbook = from.getData();
	
	// state_stack.clear(); // TODO
// 	state_stack.emplace();
// 	State &state = state_stack.top();
	
	// state.current_body = d2w.body;
// 	state.ro.lang = options.lang;
	
	
	// TODO maybe some tests
	
	// doc.reset();
	footnotes.clear();
	
// 	WordBuilder::body = doc.append_child("result"); // TODO remove
// 	WordBuilder::body = result_doc.append_child("result");
	
	
/*
 * Quotation marks:
 * ASCII: 34 (dec) 22 (hex)
 * German, double, beginning: 201E (Alt + 0132)
 * German, double, end: 201C (Alt + 0147)
 * German, single, beginning: 201A (Alt + 0130)
 * German, single, end: 2018 (Alt + 0145)
 * English, double, beginning: 201C (Alt + 0147)
 * English, double, end: 201D (Alt + 0148)
 * English, single, beginning: 2018 (Alt + 0145)
 * English, single, end: 2019 (Alt + 0146)
 * French, double, beginning: 00AB (Alt + 0171)
 * French, double, end: 00BB (Alt + 0187)
 * French, single, beginning: 2039 (Alt + 0139)
 * French, single, end: 203A (Alt + 0155)
 */
	if (options.b_double_quotes == "\"" && options.e_double_quotes == "\"")
	{
		if (options.lang == "de-DE")
		{
			options.b_double_quotes = "\u201E";
			options.e_double_quotes = "\u201C";
			options.b_single_quotes = "\u201A";
			options.e_single_quotes = "\u2018";
		}
	}
	
	result_doc.reset();
	pugi::xml_node result_node = result_doc.append_child("result");
	
	db_root_loop(result_node, from.getData().root());
	

	
	process_bookmarks();
	
	current_db = nullptr;
	return result_node;
}

Docbook2Word::BlockState::~BlockState()
{
	// remove annoying space characters at the end of a line
	// (it is a workaround to avoid more complicated state handling)
	
	if (!eat_space || !p) return;
	
	pugi::xml_node last = p.last_child();
	if (strcmp(last.name(), "w:r")) return;
	
	pugi::xml_node txt_node = last.last_child();
	if (strcmp(txt_node.name(), "w:t")) return;
	
	pugi::xml_text xmltext = txt_node.text();
	std::string txt = xmltext.get();
	WordBuilder::rtrim(txt);
	xmltext.set(txt.c_str());
}


std::string Docbook2Word::cleanAnchorName(const std::string& name)
{
	// use this function ONLY FOR ANCHOR CREATION! Then an alias from the orinal to the
	// cleaned (truncated) anchor has to be created. Because of that, an already existing
	// anchor can be found by the alias name. Therefore do not use the function when
	// searching an anchor.
	
	// anchor names (ids) must not be longer than 40 characters in word. Or maybe 38?
	// here we use 30, to be able to append numbers, if an other anchor with the
	// same name (after truncation) already exists.
	// Also it must not start with a number and so on...
	
	bool make_hidden = false; // if the changes are fundamental, hide the bookmark in word
	
	std::string base(name);
	if (base.length() > 30)
	{
		base.resize(30);
		make_hidden = true;
	}
	
	// replace special characters
	for (char& i : base)
	{
		if (
			(i < 0x30 || i > 0x39) && // 0-9
			(i < 0x41 || i > 0x5A) && // A-Z
			(i < 0x61 || i > 0x7A) && // A-Z
			i != '_'
		) i = '_';
	}
	
	// must not start with a number
	if (*base.begin() >= 0x30 && *base.begin() <= 0x39) base = '_' + base; // this also hides the bookmark
	
	// make the string distinct to the already existing names
	std::string result(base);
	int append = 0; // number to append
	while(anchor_props.find(result) != anchor_props.end())
	{
		result = base + std::to_string(append++);
		if (append > 999999) return std::string();
		make_hidden = true;
	}
	
	if (make_hidden && *result.begin() != '_') result = '_' + result;
	
	return result;
}

Docbook2Word::AnchorProperties* Docbook2Word::registerAnchor(const std::string& name, const std::string& alias)
{
	std::string use_that_name = name;
	if (use_that_name.empty()) use_that_name = alias;
	if (use_that_name.empty()) return nullptr;
	
	std::string original(use_that_name);
	use_that_name = cleanAnchorName(original);
	if (use_that_name != original) anchor_aliases.emplace(std::make_pair(original, use_that_name));
	
	AnchorProperties anchor;
	anchor.name = use_that_name;
	
	auto found_unknown = unknown_anchors_aliases.find(name);
	if (found_unknown != unknown_anchors_aliases.end())
	{
		anchor = found_unknown->second;
		unknown_anchors_aliases.erase(found_unknown);
	}
	
	auto inserted = anchor_props.insert(std::make_pair(use_that_name, anchor));
	if (inserted.second) // insertion took place
	{
		AnchorProperties& result = inserted.first->second;
		// if (result.name.length() > 40) result.name.resize(40); // or maybe 38?
		if (!alias.empty() && alias != use_that_name) anchor_aliases.emplace(std::make_pair(alias, use_that_name));
		return &result;
	}
	else return nullptr;
}

Docbook2Word::AnchorProperties* Docbook2Word::getAnchor(const std::string name)
{
	std::string real_name = name;
	
	auto found_alias = anchor_aliases.find(name);
	if (found_alias != anchor_aliases.end()) real_name = found_alias->second;
	
	auto found = anchor_props.find(real_name);
	if (found != anchor_props.end()) return &(found->second);
	
	// The requested anchor is not defined until now
	auto found_unknown = unknown_anchors_aliases.find(name);
	if (found_unknown != unknown_anchors_aliases.end()) return &(found->second);
	else return nullptr;
	
// 	AnchorProperties unknown_anchor;
// 	unknown_anchor.name = name; // may be a random uuid in future
// 	auto new_one = unknown_anchors_aliases.insert(std::make_pair(name, unknown_anchor));
// 	return &(new_one.first->second);
}


void Docbook2Word::db_root_loop(pugi::xml_node w_root, pugi::xml_node db_root)
{
	// currently only (one) article is supported
	// for (pugi::xml_node i : root.children()) {}
	
	// component  level: (book?)/part/chapter/article
	
	RootState root_state;
	root_state.body = w_root;
	
	if (db_root.child("article"))
	{
		for (pugi::xml_node i : db_root.children("article")) db_outside_section_loop(i, root_state);
	}
	else db_outside_section_loop(db_root, root_state);
}

void Docbook2Word::db_outside_section_loop(pugi::xml_node node, RootState& root_state)
{
	SectionState section_state;
	section_state.root_state = &root_state;
	
	BlockState block_state;
	block_state.section_state = &section_state;
	
	for (pugi::xml_node i : node.children())
	{
		db_section_select(i, section_state) ||
		db_block_select_p(i, block_state);
		// TODO || db_select_error();
	}
}

void Docbook2Word::db_inside_section_loop(pugi::xml_node node, SectionState& section_state)
{
	BlockState block_state;
	block_state.section_state = &section_state;
	
	for (pugi::xml_node i : node.children())
	{
		db_block_select_p(i, block_state) ||
		db_section_select(i, section_state) ||
		db_block_select_misc(i, block_state);
	}
}

void Docbook2Word::db_outside_block_loop(pugi::xml_node node, SectionState& section_state) //paragraph level
{
	BlockState block_state;
	block_state.section_state = &section_state;
	db_nested_block_loop(node, block_state);
}

void Docbook2Word::db_nested_block_loop(pugi::xml_node node, BlockState& block_state) // special blocks including blocks
{
	for (pugi::xml_node i : node.children())
	{
		db_block_select_p(i, block_state) ||
		db_block_select_misc(i, block_state);
		// note that inline elements are missing here, because they cannot occur outside of blocks
	}
}

void Docbook2Word::db_inside_block_loop(pugi::xml_node node, BlockState& block_state) // paragraph level
{
	InlineState inline_state;
	inline_state.block_state = &block_state;
	inline_state.ro.lang = options.lang;
	
	for (pugi::xml_node i : node.children())
	{
		// inline elements are allowed here inside of a block
		switch (i.type())
		{
			// case pugi::node_null: // Empty (null) node handle
			// case pugi::node_document: // A document tree's absolute root
			case pugi::node_element: // Element tag, i.e. '<node/>'
			{
				if (db_inline_select(i, inline_state)) continue;
				break;
			}
			case pugi::node_pcdata: // Plain character data, i.e. 'text'
			{
				if (db_inline_select_txt(i, inline_state)) continue;
				break;
			}
			case pugi::node_cdata: // Character data, i.e. '<![CDATA[text]]>'
			{
				if (db_inline_select_txt(i, inline_state)) continue;
				break;
			}
			// case pugi::node_comment: // Comment tag, i.e. '<!-- text -->'
			case pugi::node_pi: // Processing instruction, i.e. '<?name?>'
			{
				if (db_inline_select_pi(i, inline_state)) continue;
				break;
			}
			// case pugi::node_declaration: // Document declaration, i.e. '<?xml version="1.0"?>'
			// case pugi::node_doctype: // Document type declaration, i.e. '<!DOCTYPE doc>'
			default:
				break;
		}
		
		// note that <para> cannot be nested.
		if (
			db_block_select_misc(i, block_state)
		) continue;
	}
}

void Docbook2Word::db_outside_inline_loop(pugi::xml_node node, BlockState& block_state)
{
	InlineState inline_state;
	inline_state.block_state = &block_state;
	inline_state.ro.lang = options.lang;
	
	// same as this, therefore don't repeat here:
	db_inside_inline_loop(node, inline_state);
}

void Docbook2Word::db_inside_inline_loop(pugi::xml_node node, InlineState& inline_state)
{
	for (pugi::xml_node i : node.children())
	{
		switch (i.type())
		{
			// case pugi::node_null: // Empty (null) node handle
			// case pugi::node_document: // A document tree's absolute root
			case pugi::node_element: // Element tag, i.e. '<node/>'
			{
				if (db_inline_select(i, inline_state)) continue;
				break;
			}
			case pugi::node_pcdata: // Plain character data, i.e. 'text'
			{
				if (db_inline_select_txt(i, inline_state)) continue;
				break;
			}
			case pugi::node_cdata: // Character data, i.e. '<![CDATA[text]]>'
			{
				if (db_inline_select_txt(i, inline_state)) continue;
				break;
			}
			// case pugi::node_comment: // Comment tag, i.e. '<!-- text -->'
			case pugi::node_pi: // Processing instruction, i.e. '<?name?>'
			{
				if (db_inline_select_pi(i, inline_state)) continue;
				break;
			}
			// case pugi::node_declaration: // Document declaration, i.e. '<?xml version="1.0"?>'
			// case pugi::node_doctype: // Document type declaration, i.e. '<!DOCTYPE doc>'
			default:
				break;
		}
	}
}

bool Docbook2Word::db_section_select(pugi::xml_node node, SectionState& section_state)
{
	if (node.type() != pugi::node_element) return false;
	
	const char* n = node.name();
	if (std::strcmp(n, "section") == 0 || std::strcmp(n, "sect1") == 0 || std::strcmp(n, "sect2") == 0 ||
		std::strcmp(n, "sect3") == 0 || std::strcmp(n, "sect4") == 0 || std::strcmp(n, "sect5") == 0 ||
		std::strcmp(n, "simplesect") == 0 // TODO handle separately
	)
	{
		SectionState sub_section_state(section_state);
		
		++sub_section_state.section_level;
		const std::string id = node.attribute("id").as_string();
		std::string final_anchor_name;
		DB_Meta meta = db_meta(node);
		
		AnchorProperties* ap = nullptr;
		if (meta.anchor)
		{
			ap = registerAnchor(meta.anchor.attribute("id").as_string(), id);
			if (ap)
			{
				std::string label = meta.anchor.attribute("xreflabel").as_string();
				if (label == ("[" + id + "]")) label.clear();
				if (!label.empty())
				{
					ap->label = label;
					// ap->label_by_field = (label == meta.title);
					ap->label_by_field = false;
				}
				else
				{
					ap->label = meta.title;
					ap->label_by_field = true;
				}
				ap->type = Docbook2Word::ANCHOR_HEADING;
				final_anchor_name = ap->name;
			}
		}
		else
		{
			ap = registerAnchor(id);
			if (ap)
			{
				ap->label = meta.title;
				ap->type = Docbook2Word::ANCHOR_HEADING;
				ap->label_by_field = true;
				final_anchor_name = ap->name;
			}
		}
		meta.title = cleanText(meta.title.c_str(), true);
		mkHeading(sub_section_state.root_state->body, sub_section_state.section_level, meta.title.c_str(), final_anchor_name.c_str());
		
		db_inside_section_loop(node, sub_section_state);
		return true;
	}
	else if (false && std::strcmp(n, "simplesect") == 0) // Section without a number TODO
	{
		SectionState sub_section_state(section_state);
		
		++sub_section_state.section_level;
		const std::string id = node.attribute("id").as_string(); // TODO maximum 40 characters
		DB_Meta meta = db_meta(node);
		
		AnchorProperties* ap = nullptr;
		if (meta.anchor)
		{
			ap = registerAnchor(id, meta.anchor.attribute("id").as_string());
			if (ap)
			{
				const char* label = meta.anchor.attribute("xreflabel").as_string();
				if (*label)
				{
					ap->label = label;
					ap->label_by_field = (label == meta.title);
				}
				else
				{
					ap->label = meta.title;
					ap->label_by_field = true;
				}
				ap->type = Docbook2Word::ANCHOR_HEADING;
			}
		}
		else
		{
			ap = registerAnchor(id);
			if (ap)
			{
				ap->label = meta.title;
				ap->type = Docbook2Word::ANCHOR_HEADING;
				ap->label_by_field = true;
			}
		}
		
		// TODO make bookmark here!!!
		db_inside_section_loop(node, sub_section_state);
		return true;
		
		// TODO: should not have a number (find the right style...)
	}
	return false;
}

bool Docbook2Word::db_block_select_p(pugi::xml_node node, Docbook2Word::BlockState& block_state)
{
	if (node.type() == pugi::node_cdata || node.type() == pugi::node_pcdata)
	{
		// OK, that's wired, but may happen - especially in <entry> tags of a table, if they were generated by pandoc.
		// Treat it similar as simpara, but jump directly to the text handler.
		
		BlockState sub_block_state(block_state);
		InlineState inline_state;
		inline_state.block_state = &block_state;
		inline_state.ro.lang = options.lang;
		return db_inline_select_txt(node, inline_state);
	}
	
	if (node.type() != pugi::node_element) return false;
	
	const char* n = node.name();
	if (std::strcmp(n, "para") == 0)
	{
		BlockState sub_block_state(block_state);
		// mkPar will be run if necessary at mkRunStack(). In a very specific case it is not known at this time, how the
		// paragraph style looks, because it is determined later. For example: "literallayout"
		// sub_block_state.p = mkPar(sub_block_state.section_state.root_state.body, sub_block_state.po);
		db_inside_block_loop(node, sub_block_state);
		// ensure that an empty paragraph really produces a paragraph
		// if (!sub_block_state) mkPar(sub_block_state.section_state.root_state.body, sub_block_state.po);
		return true;
	}
	else if (std::strcmp(n, "simpara") == 0) // simple paragraph, can only contain text but no block-level elements like figures
	{
		BlockState sub_block_state(block_state);
		// sub_block_state.p = mkPar(sub_block_state.section_state.root_state.body, sub_block_state.po);
		db_outside_inline_loop(node, sub_block_state); // simpara cannot contain block elements
		return true;
	}
	else if (std::strcmp(n, "formalpara") == 0) // titled paragraph: <formalpara><title/><para/></formalpara>
	{
		BlockState sub_block_state(block_state);
		DB_Meta meta = db_meta(node);
		mkParHeading(meta.title.c_str(), sub_block_state.section_state->root_state->body);
		// sub_block_state.p = mkPar(body, sub_block_state.po); // not necessary, would create additional empty par
		db_outside_block_loop(node, *sub_block_state.section_state); // because <formalpara> cannot contain inline elements. Allowed children are for example <para>
		return true;
	}

	return false;
	
	
	
}

bool Docbook2Word::db_block_select_misc(pugi::xml_node node, Docbook2Word::BlockState& block_state)
{
	if (node.type() != pugi::node_element) return false;
	
	// For many cases an independent root/body stack is needed, because the
	// body is replaced by a sub body. It can be used, or not.
	RootState sub_root_state(*block_state.section_state->root_state);
	SectionState sub_section_state(*block_state.section_state);
	BlockState sub_block_state(block_state);
	sub_block_state.section_state = &sub_section_state;
	sub_section_state.root_state = &sub_root_state;
	
	const char* n = node.name();
	if (std::strcmp(n, "table") == 0
		|| std::strcmp(n, "informaltable") == 0 // a table without a title (but can include header and footer like table)
	)
	{
		// not necessary anymore, because after each table a empty line is inserted
// 		const char* previous = node.previous_sibling().name();
// 		if (strcmp(previous, "table") == 0 || strcmp(previous, "informaltable") == 0)
// 		{
// 			ParOptions po;
// 			mkPar(block_state.section_state->root_state->body, po);
// 		}
		
		pugi::xml_node &body = sub_block_state.section_state->root_state->body;
		sub_block_state.p = pugi::xml_node();
		pugi::xml_node tbl = body.append_child("w:tbl");
		body = tbl;
		db_nested_block_loop(node, sub_block_state);
		
		
		// Always insert an empty line afterwards
		ParOptions po;
		po.style = word_template.getParStyleID(WordFile::WordStylePar::S_NO_SPACE);
		mkPar(block_state.section_state->root_state->body, po);
	}

	else if (std::strcmp(n, "tgroup") == 0)
	{
		// do nothing here
		db_nested_block_loop(node, block_state);
	}
	else if (std::strcmp(n, "tbody") == 0)
	{
		// do nothing here
		db_nested_block_loop(node, block_state);
	}
	else if (std::strcmp(n, "thead") == 0)
	{
		// do nothing here
		db_nested_block_loop(node, block_state);
	}
	else if (std::strcmp(n, "row") == 0)
	{
		pugi::xml_node &body = sub_block_state.section_state->root_state->body;
		sub_block_state.p = pugi::xml_node();
		pugi::xml_node tr = body.append_child("w:tr");
		body = tr;
		db_nested_block_loop(node, sub_block_state);
	}
	else if (std::strcmp(n, "entry") == 0) // one cell in a row
	{
		pugi::xml_node &body = sub_block_state.section_state->root_state->body;
		sub_block_state.po.style = word_template.getParStyleID(WordFile::S_NO_SPACE);
		sub_block_state.p = pugi::xml_node();
		pugi::xml_node tc = body.append_child("w:tc");
		body = tc;
		db_nested_block_loop(node, sub_block_state);
		// Word does not like it, when "w:tc" keeps empty. A paragraph is mandatory
		if (!tc.first_child()) mkPar(tc, sub_block_state.po);
	}
	else if (std::strcmp(n, "itemizedlist") == 0)
	{
		db_block_itemizedList(node, block_state);
		return true;
	}
	else if (std::strcmp(n, "orderedlist") == 0)
	{
		db_block_orderedList(node, block_state);
		return true;
	}
	else if (std::strcmp(n, "screen") == 0 || std::strcmp(n, "programlisting") == 0)
	{
		db_block_screen(node, block_state);
		
		// Always insert an empty line afterwards
		ParOptions po;
		po.style = word_template.getParStyleID(WordFile::WordStylePar::S_NO_SPACE);
		mkPar(block_state.section_state->root_state->body, po);
		
		return true;
	}
	else if (std::strcmp(n, "calloutlist") == 0)
	{
		db_block_calloutlist(node, block_state);
		return true;
	}
	else if (std::strcmp(n, "informalfigure") == 0 || std::strcmp(n, "figure") == 0)
	{
		db_block_figure(node, block_state);
		return true;
	}
	
	
	return false;

}

bool Docbook2Word::db_inline_select(pugi::xml_node node, InlineState& inline_state)
{
	if (node.type() != pugi::node_element) return false;
	
	const char* n = node.name();
	if (std::strcmp(n, "phrase") == 0)
	{
		inline_state.r = pugi::xml_node(); // w:r cannot be reused and has to be created before and after this context
		InlineState sub_inline_state(inline_state);
		
		const char* role = node.attribute("role").as_string();
		
		size_t len = std::strlen(role);
		char* cache = new char[len+1];
		std::memcpy(cache, role, len+1);
		
		char *token = std::strtok(cache, " ");
		while (token)
		{
			std::string strtoken(token);
			
			size_t found = strtoken.find('-');
			bool background = false;
			if (found != std::string::npos && strcmp(strtoken.c_str() + found + 1, "background") == 0)
			{
				background = true;
				strtoken = std::string(strtoken.c_str(), found);
			}
			
			if (word_template.isValidStyleId(strtoken)) sub_inline_state.ro.style = strtoken;
			else
			{
				if (strtoken == "strong") sub_inline_state.ro.charstyle |= CharStyle::BOLD;
				else if (strtoken == "bold") sub_inline_state.ro.charstyle |= CharStyle::BOLD;
				else if (background) sub_inline_state.ro.highlight = strtoken;
				else sub_inline_state.ro.colour = strtoken; // for example role=red TODO check valid colours
			}
			
			token = std::strtok(NULL, " ");
		}
		
		
		
		
		


		db_inside_inline_loop(node, sub_inline_state);
		return true;
	}
	else if (std::strcmp(n, "emphasis") == 0)
	{
		inline_state.r = pugi::xml_node(); // w:r cannot be reused and has to be created before and after this context
		InlineState sub_inline_state(inline_state);
		
		const char* role = node.attribute("role").as_string();
		if (std::strcmp(role, "strong") == 0) sub_inline_state.ro.charstyle |= CharStyle::BOLD;
		else if (std::strcmp(role, "bold") == 0) sub_inline_state.ro.charstyle |= CharStyle::BOLD;
		else sub_inline_state.ro.charstyle |= CharStyle::ITALIC;
		
		db_inside_inline_loop(node, sub_inline_state);
		return true;
	}
	else if (std::strcmp(n, "superscript") == 0)
	{
		inline_state.r = pugi::xml_node(); // w:r cannot be reused and has to be created before and after this context
		InlineState sub_inline_state(inline_state);
		
		sub_inline_state.ro.charstyle |= CharStyle::SUPER;
		
		db_inside_inline_loop(node, sub_inline_state);
		return true;
	}
	else if (std::strcmp(n, "subscript") == 0)
	{
		inline_state.r = pugi::xml_node(); // w:r cannot be reused and has to be created before and after this context
		InlineState sub_inline_state(inline_state);
		
		sub_inline_state.ro.charstyle |= CharStyle::SUB;
		
		db_inside_inline_loop(node, sub_inline_state);
		return true;
	}
	else if (std::strcmp(n, "literal") == 0) // inline text (without linebreaks), just to highlight as somthing literal (like commands)
	{
		inline_state.r = pugi::xml_node(); // w:r cannot be reused and has to be created before and after this context
		InlineState sub_inline_state(inline_state);
		
		bool old_literally = sub_inline_state.block_state->literally;
		sub_inline_state.block_state->literally = true;
		sub_inline_state.ro.charstyle |= CharStyle::NO_PROOF;
		sub_inline_state.ro.style = word_template.getTxtStyleID(WordFile::WordStyleTxt::S_SOURCECODE_TXT);
		db_inside_inline_loop(node, sub_inline_state);
		sub_inline_state.block_state->literally = old_literally;
		return true;
	}
	else if (std::strcmp(n, "literallayout") == 0) // block of text with 1:1 linebreaks and white space
	{
		const char* class_attrib = node.attribute("class").as_string();
		bool monospaced = false;
		if (strcmp(class_attrib, "monospaced") == 0) monospaced = true;
		
		BlockState sub_block_state(*inline_state.block_state);
		InlineState sub_inline_state(inline_state);
		sub_inline_state.block_state = &sub_block_state;
		
		sub_block_state.literally = true;
		sub_inline_state.ro.charstyle |= CharStyle::NO_PROOF;
		if (monospaced) sub_block_state.po.style = word_template.getParStyleID(WordFile::WordStylePar::S_SOURCECODE_PAR);
		sub_block_state.p = mkPar(sub_block_state.section_state->root_state->body, sub_block_state.po);
		db_outside_inline_loop(node, sub_block_state); // simpara cannot contain block elements
		return true;
	}


	else if (std::strcmp(n, "footnote") == 0)
	{
		pugi::xml_node last_wr = inline_state.r;
		inline_state.r = pugi::xml_node();
		db_inline_footnote(node, inline_state, last_wr);
		return true;
	}
	else if (std::strcmp(n, "footnoteref") == 0)
	{
		if (options.rm_lead_footnote_blank)
		{
			pugi::xml_node found_wt_node = inline_state.r.last_child();
			while (found_wt_node && strcmp(found_wt_node.name(), "w:t")) found_wt_node = found_wt_node.previous_sibling();
			pugi::xml_text xmltext = found_wt_node.text();
			std::string strtext = std::string(xmltext.get());
			rtrim(strtext);
			xmltext.set(strtext.c_str());
		}
		
		inline_state.r = pugi::xml_node();
		// TODO Error if empty docbook_id
		const std::string docbook_id = node.attribute("linkend").as_string();
		int footnote_id;
		
		auto found = footnotes.find(docbook_id);
		if (found != footnotes.end()) footnote_id = found->second;
		else
		{
			++private_opts.next_footnote_id;
			footnotes.emplace(std::make_pair(docbook_id, footnote_id));
		}
		
		RunOptions ro;
		ro.style = word_template.getTxtStyleID(WordFile::WordStyleTxt::S_FOOTNOTE_REF);
		
		// std::string bname = std::string("footnote_") + docbook_id + std::to_string(footnote_id);
		std::string bname = std::string("footnote_") + std::to_string(footnote_id);
		mkFootnoteRefNr(inline_state.block_state->p, bname.c_str(), "###", ro);
		return true;
	}
	else if (std::strcmp(n, "anchor") == 0)
	{
		const char* linkend = node.attribute("linkend").as_string();
		const char* xreflabel = node.attribute("xreflabel").as_string();
		
		AnchorProperties* ap = registerAnchor(linkend);
		if (ap)
		{
			ap->label = xreflabel;
			ap->type = Docbook2Word::Docbook2Word::ANCHOR_OTHER;
			
			pugi::xml_node r = mkRun(inline_state.block_state->p, inline_state.ro);
			inline_state.r = pugi::xml_node();
			mkBookmark(r, r, linkend);
		}
		
		return true;
	}
	else if (
		std::strcmp(n, "link") == 0 ||
		std::strcmp(n, "xref") == 0 || // cannot contain text, only endterm, otherwise use xreflabel from the destination
		std::strcmp(n, "ulink") == 0
	)
	{
		// only one of the following:
		const char* linkend = node.attribute("linkend").as_string();
		const char* url = node.attribute("xlink:href").as_string();
		// const char* endterm = node.attribute("endterm").as_string(); // get the text from the object with that id
		
		std::string txt = extractTextRecursive(node);
		if (txt.empty()) txt = "###";
		// does not work as expected:
		//if (!inline_state.block_state->literally) txt = " " + txt + " "; // will be eaten if redundant
		txt = cleanText(txt.c_str(), inline_state.block_state->eat_space);
		
		inline_state.r = pugi::xml_node(); // enforce a new run
		mkRunStack(inline_state);
		pugi::xml_node& r = inline_state.r;
		// pugi::xml_node r = mkRun(inline_state.block_state->p, inline_state.ro);
		mkTxt(r, std::string(txt).c_str(), false);
		
		
		BookmarkProperties bp;
		bp.src = node;
		bp.dst = r;
		bp.ro = inline_state.ro;
		
		bookmarks_to_process.push_back(bp);
		
		inline_state.r = pugi::xml_node();
		return true;
	}
/*
	else if (std::strcmp(n, "ulink") == 0)
	{
		const char* url = node.attribute("url").as_string();
		std::string rid = mkMediaRelationship(word_template.getRelsDoc(), "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink", url, "External");
		pugi::xml_node hyperlink = inline_state.block_state->p.append_child("w:hyperlink");
		hyperlink.append_attribute("r:id") = rid.c_str();
		hyperlink.append_attribute("w:history") = "1";
		hyperlink.append_attribute("w:tooltip") = url;
		// hyperlink.append_attribute("w:docLocation") = "sub destination in the destination, for example table";
		
		RunOptions ro(inline_state.ro);
		ro.style = word_template.getTxtStyleID(WordFile::S_HYPERLINK);
		pugi::xml_node r = mkRun(hyperlink, ro);
		std::string txt = extractTextRecursive(node);
		txt = cleanText(txt.c_str(), inline_state.block_state->eat_space);
		mkTxt(r, txt.c_str(), false);
		inline_state.r = pugi::xml_node();
		return true;
	}
*/
	else if (std::strcmp(n, "inlinemediaobject") == 0)
	{
		// note that "inlinemediaobject" is not on the same level as informalfigure or figure, but db_block_figure
		// takes care of this fact. Currently it is not wanted to generate inline figures. Therefore the current run
		// and paragraph are invalidated afterwards.
		db_block_figure(node, *inline_state.block_state);
		
		inline_state.r = pugi::xml_node();
		inline_state.block_state->p = pugi::xml_node();
		return true;
	}
	
}

void Docbook2Word::process_bookmarks()
{
	for (const BookmarkProperties& i : bookmarks_to_process)
	{
		const char* placeholder_txt = i.dst.child("w:t").first_child().text().get();
		bool starts_with_space = *placeholder_txt == ' ';
		bool ends_with_space = *(placeholder_txt + strlen(placeholder_txt)) == ' ';
		
		
		const char* n = i.src.name();
		if (std::strcmp(n, "link") == 0)
		{
			// only one of the following:
			const char* linkend = i.src.attribute("linkend").as_string();
			const char* url = i.src.attribute("xlink:href").as_string();
			// const char* endterm = i.src.attribute("endterm").as_string(); // get the text from the object with that id
			
			std::string txt = extractTextRecursive(i.src);
			txt = cleanText(txt.c_str(), !starts_with_space);
			if (!ends_with_space) rtrim(txt);

			if (*linkend)
			{
				AnchorProperties* ap = getAnchor(linkend);
				if (ap)
				{
					if (txt == "0") // use the number of the numbered destination object
					{
						if (ap->type == Docbook2Word::ANCHOR_FIGURE)
						{
							// Get two nested fields, something like that: { quote { REF _Ref12345 \h } \* arabic }
							
							// Code part A
							pugi::xml_node code_A = mkRunBefore(i.dst, i.ro);
							pugi::xml_node code_instr_A = code_A.append_child("w:instrText");
							code_instr_A.append_attribute("xml:space") = "preserve";
							code_instr_A.append_child(pugi::node_pcdata).text().set(" quote ");
							
							// the following inner field is used as part B of the code
							pugi::xml_node empty_r = mkRunAfter(code_A, i.ro);
							std::string insert_txt = "###";
							if (starts_with_space) insert_txt = " " + insert_txt;
							if (ends_with_space) insert_txt += " ";
							mkTxt(empty_r, insert_txt.c_str(), false);
							std::pair<pugi::xml_node, pugi::xml_node> inner = mkField(empty_r, empty_r, " REF \"" + ap->name + "\" \\h ", i.ro, false, true);
							
							// Code part B
							pugi::xml_node code_B = mkRunAfter(inner.second, i.ro);
							pugi::xml_node code_instr_B = code_B.append_child("w:instrText");
							code_instr_B.append_attribute("xml:space") = "preserve";
							code_instr_B.append_child(pugi::node_pcdata).text().set(" \\* arabic ");
							
							// Text content
							pugi::xml_node r = i.dst;
							r.remove_child("w:t");
							mkTxt(r, insert_txt.c_str(), false);
							
							mkField(code_A, code_B, r, i.ro, false, true);
							
							// TODO if possible, make clickable link
						}
						else
						{
							pugi::xml_node r = i.dst;
							r.remove_child("w:t");
							
							std::string insert_txt = "###";
							if (starts_with_space) insert_txt = " " + insert_txt;
							if (ends_with_space) insert_txt += " ";
							mkTxt(r, insert_txt.c_str(), false);
							
							mkField(r, r, " REF \"" + ap->name + "\" \\r \\h ", i.ro, false, true);
						}
					}
					else
					{
						bool use_dynamic_label = ap->label_by_field && txt.empty();
						
						if (txt.empty()) txt = ap->label; // very unlikely, because xref would be used instead of link in this case
						if (txt.empty()) txt = "###";
						
						if (use_dynamic_label) // use the text of the origin (same as xref)
						{
							pugi::xml_node r = i.dst;
							r.remove_child("w:t");
							mkTxt(r, txt.c_str(), false);
							mkField(r, r, " REF \"" + ap->name + "\" \\h ", i.ro, false, true);
						}
						else
						{
							pugi::xml_node hyperlink = i.dst.parent().insert_child_before("w:hyperlink", i.dst);
							hyperlink.append_attribute("w:anchor") = ap->name.c_str();
							hyperlink.append_attribute("w:history") = "0"; // do not add to the list of viewed links
							if (!txt.empty() && txt != ap->label) hyperlink.append_attribute("w:tooltip") = ap->label.c_str();
							// destination in the target, if external: w:docLocation="table"
							
							RunOptions ro(i.ro);
							// ro.style = word_template.getTxtStyleID(WordFile::S_HYPERLINK); // do not use the Hyperlink style here
							pugi::xml_node r = mkRun(hyperlink, ro);
							mkTxt(r, txt.c_str(), false);
							
							i.dst.parent().remove_child(i.dst);
						}
					}
				}
				else // the ID is already existing or similar problems - just insert the text
				{
					pugi::xml_node r = i.dst;
					r.remove_child("w:t");
					
					std::string insert_txt = "###";
					if (starts_with_space) insert_txt = " " + insert_txt;
					if (ends_with_space) insert_txt += " ";
					mkTxt(r, insert_txt.c_str(), false);
					
					// TODO should cause a warning
				}
			}
			else if (*url)
			{
				std::string rid = mkMediaRelationship(word_template.getRelsDoc(), "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink", url, "External");
				pugi::xml_node hyperlink = i.dst.parent().insert_child_before("w:hyperlink", i.dst);
				hyperlink.append_attribute("r:id") = rid.c_str();
				hyperlink.append_attribute("w:history") = "1";
				hyperlink.append_attribute("w:tooltip") = url;
				// hyperlink.append_attribute("w:docLocation") = "sub destination in the destination, for example table";
				
				RunOptions ro(i.ro);
				ro.style = word_template.getTxtStyleID(WordFile::S_HYPERLINK);
				pugi::xml_node r = mkRun(hyperlink, ro);
				mkTxt(r, txt.c_str(), false);
				
				i.dst.parent().remove_child(i.dst);
			}
			

		}
		else if (std::strcmp(n, "xref") == 0) // cannot contain text, only endterm, otherwise use xreflabel from the destination
		{
			const char* linkend = i.src.attribute("linkend").as_string();
			// const char* endterm = i.src.attribute("endterm").as_string(); // get the text from the object with that id instead of linkend id
			
			AnchorProperties* ap = getAnchor(linkend);
			if (ap)
			{
				std::string txt = ap->label;
				txt = cleanText(txt.c_str(), true);
				
				if (txt.empty())
				{
					std::string insert_txt = "###";
					if (starts_with_space) insert_txt = " " + insert_txt;
					if (ends_with_space) insert_txt += " ";
					txt = insert_txt;
				}
				
				pugi::xml_node r = i.dst;
				r.remove_child("w:t");
				mkTxt(r, txt.c_str(), false);
				mkField(r, r, " REF \"" + ap->name + "\" \\h ", i.ro, false, true);
			}
			else // the ID is already existing or similar problems - just insert the text
			{
				// already done
			}
			

		}
		else if (std::strcmp(n, "ulink") == 0)
		{
			const char* url = i.src.attribute("url").as_string();
			std::string rid = mkMediaRelationship(word_template.getRelsDoc(), "http://schemas.openxmlformats.org/officeDocument/2006/relationships/hyperlink", url, "External");
			pugi::xml_node hyperlink = i.dst.parent().insert_child_before("w:hyperlink", i.dst);
			hyperlink.append_attribute("r:id") = rid.c_str();
			hyperlink.append_attribute("w:history") = "1";
			hyperlink.append_attribute("w:tooltip") = url;
			// hyperlink.append_attribute("w:docLocation") = "sub destination in the destination, for example table";
			
			RunOptions ro(i.ro);
			ro.style = word_template.getTxtStyleID(WordFile::S_HYPERLINK);
			pugi::xml_node r = mkRun(hyperlink, ro);
			
			std::string txt = extractTextRecursive(i.src);
			txt = cleanText(txt.c_str(), true);
			txt = cleanText(txt.c_str(), !starts_with_space);
			if (!ends_with_space) rtrim(txt);
			
			mkTxt(r, txt.c_str(), false);
			i.dst.parent().remove_child(i.dst);
		}
	}
	bookmarks_to_process.clear();
}


bool Docbook2Word::db_inline_select_txt(pugi::xml_node node, InlineState& inline_state)
{
	std::string txt{node.text().get()};
	
	if (inline_state.block_state->literally)
	{
		std::string::size_type str_from = 0;
		std::string::size_type str_to = 0;
		
		while (true)
		{
			str_to = txt.find("\n", str_from);
			std::string str_token {
				str_to == std::string::npos ?
				txt.substr(str_from, str_to) :
				txt.substr(str_from, str_to-str_from)
			};
			
			// remove that annoying windows-only newline characters from beginning
			std::string::size_type remove_count = 0;
			while (remove_count < str_token.length() && str_token[remove_count] == '\r') ++remove_count;
			if (remove_count) str_token.erase(0, remove_count);
			
			// same from the end
			remove_count = str_token.length();
			while (remove_count > 0 && str_token[remove_count-1] == '\r') --remove_count;
			str_token.resize(remove_count);
			
			mkRunStack(inline_state);
			mkTxt(inline_state.r, str_token.c_str(), false, true, true);
			
			if (str_to == std::string::npos) break;
			else
			{
				// enforce new paragraph next time
				inline_state.block_state->p = pugi::xml_node();
				inline_state.r = pugi::xml_node();
			}
			
			str_from = str_to+1;
		}
		
		
		// without the token stuff it would be:
		// mkRunStack(inline_state);
		// mkTxt(inline_state.r, txt, false, true, true);
		
		inline_state.block_state->eat_space = false; // keep the next following space, if it exists
	}
	else
	{
		std::string s_txt = cleanText(txt.c_str(), inline_state.block_state->eat_space);
		
		/*
		while (*txt != 0 && (*txt == '\r' || *txt == '\n')) ++txt; // remove newlines at the beginning
		std::string s_txt = std::string(txt);
		while (s_txt.back() == '\r' || s_txt.back() == '\n') s_txt.pop_back(); // remove newlines at the end
		
	// 	// size_t len = std::strlen(txt);
	// 	// const bool preserve_space = len && (txt[0] == ' ' || txt[len-1] == ' ');
	// 	const bool preserve_space = s_txt.length() && (s_txt.front() == ' ' || s_txt.back() == ' ');
		*/
		
		mkRunStack(inline_state);
		mkTxt(inline_state.r, s_txt.c_str(), false);
	}
	
	

	return true;
}

bool Docbook2Word::db_inline_select_pi(pugi::xml_node node, InlineState& inline_state)
{
	assert(node.type() == pugi::node_pi);
	const char* n = node.name();
	// note: some of these types cannot occur in inline context, but it should be a more or less complete list
	
	if (std::strcmp(n, "asciidoc-toc") == 0) return true;
	else if (std::strcmp(n, "asciidoc-numbered") == 0) return true;
	else if (std::strcmp(n, "asciidoc-br") == 0)
	{
		mkRunStack(inline_state);
		pugi::xml_node br = inline_state.r.append_child("w:br");
		// TODO insert right justified tabstop before the linebreak, to ensure, that justified text is not bend to the end of the line
		return true;
	}
	else if (std::strcmp(n, "asciidoc-pagebreak") == 0)
	{
		mkRunStack(inline_state);
		pugi::xml_node br = inline_state.r.append_child("w:br");
		br.append_attribute("w:type") = "page";
		return true;
	}
	else if (std::strcmp(n, "asciidoc-hr") == 0) return true;
	
	return false;
}

bool Docbook2Word::db_inline_footnote(pugi::xml_node node, Docbook2Word::InlineState& inline_state, pugi::xml_node last_wr)
{
	if (options.rm_lead_footnote_blank)
	{
		pugi::xml_node found_wt_node = last_wr.last_child();
		while (found_wt_node && strcmp(found_wt_node.name(), "w:t")) found_wt_node = found_wt_node.previous_sibling();
		pugi::xml_text xmltext = found_wt_node.text();
		std::string strtext = std::string(xmltext.get());
		rtrim(strtext);
		xmltext.set(strtext.c_str());
	}
	
	
	
	const std::string docbook_id = node.attribute("id").as_string();
	
	inline_state.block_state->eat_space = false; // after the footnote number
	RootState fn_root(*inline_state.block_state->section_state->root_state);
	SectionState fn_sec(*inline_state.block_state->section_state);
	BlockState fn_p; // do not copy anything here
	fn_p.section_state = &fn_sec;
	fn_sec.root_state = &fn_root;
	
	int footnote_id = private_opts.next_footnote_id;
	
	if(!docbook_id.empty())
	{
		auto found = footnotes.find(docbook_id);
		if (found != footnotes.end()) footnote_id = found->second;
		else
		{
			++private_opts.next_footnote_id;
			footnotes.emplace(std::make_pair(docbook_id, footnote_id));
		}
	}
	else ++private_opts.next_footnote_id;
	
	RunOptions ro;
	ro.style = word_template.getTxtStyleID(WordFile::WordStyleTxt::S_FOOTNOTE_REF);
	const std::string pstyle = word_template.getParStyleID(WordFile::WordStylePar::S_FOOTNOTE_PAR);
	fn_p.p = mkFootnoteP(word_template.getFootnoteDoc(), footnote_id, pstyle, ro);
	fn_p.section_state->root_state->body = fn_p.p.parent();
	
	// std::string bname = std::string("footnote_") + docbook_id + std::to_string(footnote_id);
	std::string bname = std::string("footnote_") + std::to_string(footnote_id);
	mkFootnoteNr(inline_state.block_state->p, footnote_id, ro, bname.c_str());
	
	bool first_time = true;
	for (pugi::xml_node i : node.children())
	{
		if (node.type() != pugi::node_element) continue;
		
		const char* n = i.name();
		
		if (first_time)
		{
			first_time = false;

			if (std::strcmp(n, "para") == 0) db_inside_block_loop(i, fn_p);
			else if (std::strcmp(n, "simpara") == 0) db_outside_inline_loop(i, fn_p);
		}
		else db_block_select_p(i, fn_p); // but a footnote with more than one paragraph would be very seldom
	}
}






Docbook2Word::DB_Meta Docbook2Word::db_meta(pugi::xml_node node)
{
	DB_Meta result;
	pugi::xml_node title = node.child("title");
	result.title = extractTextRecursive(title);
// 	for (pugi::xml_node i : title.children())
// 		if (i.type() == pugi::xml_node_type::node_pcdata || i.type() == pugi::xml_node_type::node_cdata) result.title += i.text().get();
	// result.title = title.text().get();
	trim (result.title);
	result.anchor = title.child("anchor");
	// result.subtitle = node.child("subtitle").text().get();
	return result;
}

bool Docbook2Word::isListCompact(pugi::xml_node node, int level) const
{
	// criteria:
	// a) the list is flat (only one level)
	// b) the elements contain only text, existing in only one <simpara> (implies a)
	// c) each element has not more than 65 characters
	// d) text inside of footmarks is not counted, but every other element is
	
	const char * spacing = node.attribute("spacing").as_string();
	if (!strcmp(spacing, "compact")) return true;
	if (level > 0) return false;
	if (!strcmp(spacing, "normal")) return false;

	// this function calls itself recursively to search the whole xml subtree, and count the text node characters.
	// if true, then the limit was exceeded,
	std::function<bool (pugi::xml_node parent, int &counter, const int limit)> deep_count_char;
	deep_count_char = [&deep_count_char] (pugi::xml_node parent, int &counter, const int limit) -> bool
	{
		for (pugi::xml_node i : parent.children())
		{
			switch (i.type())
			{
				case pugi::node_element:
					// footnotes are no problem (strcmp would be 0 / false in this case, but true if it isn't a footnote)
					if (strcmp(i.name(), "footnote") && deep_count_char(i, counter, limit)) return true;
					break;
				case pugi::node_cdata:
				case pugi::node_pcdata:
					counter += strlen(parent.text().get());
					if (counter > limit) return true;
					break;
			}
		}
	};
	
	for (pugi::xml_node i : node.children("listitem"))
	{
		int counter = 0;
		if (
			i.first_child() != i.last_child() || // more than just one child node
			strcmp(i.first_child().name(), "simpara") || // other than just <simpara>
			deep_count_char(i.first_child(), counter, 65) // <simpara> with too many text
		) return false;
	}
	
	return true;
}

void Docbook2Word::db_block_itemizedList(pugi::xml_node node, Docbook2Word::BlockState& block_state)
{
	ListState list_state;
	list_state.block_state = &block_state;
	db_itemizedList(node, list_state);
}

void Docbook2Word::db_block_orderedList(pugi::xml_node node, Docbook2Word::BlockState& block_state)
{
	ListState list_state;
	list_state.block_state = &block_state;
	db_orderedList(node, list_state);
}

void Docbook2Word::db_itemizedList(pugi::xml_node node, Docbook2Word::ListState& list_state)
{
	++list_state.indent_level;
	++list_state.item_indent_level;
	
	if (list_state.indent_level == 0)
	{

		DB_Meta meta = db_meta(node);
		if (!meta.title.empty()) mkParHeading(meta.title.c_str(), list_state.block_state->section_state->root_state->body);
		else
		{
			const char* previous = node.previous_sibling().name();
			if (strcmp(previous, "itemizedlist") == 0 || strcmp(previous, "orderedlist") == 0)
			{
				ParOptions po;
				mkPar(list_state.block_state->section_state->root_state->body, po);
			}
		}
	}

	/*
	 * Theree are three possible itemize "styles":
	 *    * The internal default style which is applied, if all other methods fail (internal_scheme)
	 *    * A current, user defined style, built up by user requested bullet styles (list_state.schme), but initialised with the internal default style
	 *    * The default itemize style in the word document (style/style_id). If present it defines the font/paragraph style, but not always the itemize style.
	 *      If the itemize style is defined, it is just used as default. But it is not evaluated, and if the user requests an custom bullet
	 *      style, it is not possible to compare it with the style in the word document, and to use this style if they are identical.
	*/
	

	
	// levels > 9 (1-9 or 0-8) cannot be treated ; TODO this should cause at least a warning, because it is not allowed!
	assert(list_state.indent_level <= 8 && list_state.item_indent_level <= 8);
	if (list_state.indent_level > 8)
	{
		// just write the content as paragraphs
		for (pugi::xml_node i : node.children())
		{
			if (i.type() != pugi::node_element) continue;
			BlockState sub_block_state(*list_state.block_state);
			sub_block_state.p = mkPar(sub_block_state.section_state->root_state->body, sub_block_state.po);
			db_nested_block_loop(i, sub_block_state);
		}
		return;
	}
	
	const bool compact = isListCompact(node, list_state.indent_level);
	const WordFile::WordStylePar &style = compact ? private_opts.itemize_compact_style : private_opts.itemize_normal_style;
	const std::string style_id = word_template.getParStyleID(style);
	int &numId = compact ? private_opts.itemize_compact_numId : private_opts.itemize_normal_numId;
	
	// use and keep an internal default scheme
	const static WordBuilder::ListScheme internal_scheme = createDefaultItemizeScheme();
	static int internal_scheme_id = -1;
	// static bool internal_scheme_implemented = false; // not necessary, because the internal scheme has to be implemented immediately after declaration always!
	
	
	// now get information about the user requested style
	std::string mark = node.attribute("mark").as_string(); // like "*"
	
	int use_numId = -1;
	
	if (!mark.empty()) // the user requests a specific bullet style
	{
		if ( // can the current style being used again (because identical to the requested)?
			list_state.scheme_id >= 0 && // already declared
			list_state.scheme.size > list_state.indent_level &&
			!list_state.scheme.levels[list_state.indent_level].is_enumeration &&
			list_state.scheme.levels[list_state.indent_level].numFmt == "bullet" &&
			list_state.scheme.levels[list_state.indent_level].lvlText == mark)
		{
			use_numId = list_state.scheme_id;
		}
		else if ( // can the internal default style be used (because identical to the requested)?
			list_state.scheme_id < 0 && !list_state.implemented && // if an user defined style is already in use, and can be extended, then do not use the internal style
			internal_scheme_id >= 0 && // already declared and implemented - otherwise it is better to create a new custom style
			// internal_scheme.size > list_state.indent_level && // this test is not really necessary (currently), because the internal scheme fits always this criteria
			// !internal_scheme.levels[list_state.indent_level].is_enumeration && // not necessary too
			// internal_scheme.levels[list_state.indent_level].numFmt == "bullet" && // not necessary too
			internal_scheme.levels[list_state.indent_level].lvlText == mark)
		{
// disabled, because the else if includes a test, that the scheme is already declared and implemented
// 			// the requested style is identical to the internal style
// 			if (internal_scheme_id < 0) // implement because currently not implemented
// 			{
// 				std::pair<int, pugi::xml_node> new_scheme = declareNewListScheme();
// 				internal_scheme_id = new_scheme.first;
// 				implementListScheme(new_scheme.second, internal_scheme);
// 			}
			use_numId = internal_scheme_id;
		}
		else // modify the current style as requested
		{
			if (list_state.implemented) // an already implemented style cannot be extended. Create a new one
			{
				list_state.scheme_id = -1;
				list_state.implemented = false;
			}
			
			if (list_state.scheme_id < 0) // an empty style needs to be declared, because an ID is needed
			{
				std::pair<int, pugi::xml_node> new_scheme = declareNewListScheme();
				list_state.scheme_id = new_scheme.first;
				list_state.abstract_scheme_node = new_scheme.second;
				list_state.scheme = internal_scheme; // initialise to default, but each level should be created explicitly
				list_state.scheme.nsid = "";
				list_state.scheme.size = 0;
			}

			
			// initialise the levels which were not used until now
			int shift_levels = list_state.indent_level - list_state.item_indent_level;
			for (int i = list_state.scheme.size ; i <= list_state.indent_level ; ++i)
			{
				initDefaultItemizeLevel(list_state.scheme.levels[i], (i-shift_levels+9)%9, i);
				// initDefaultItemizeLevel(list_state.scheme.levels[i], i);
			}
			list_state.scheme.levels[list_state.indent_level].lvlText = mark;
			list_state.scheme.size = list_state.indent_level + 1;
			
			use_numId = list_state.scheme_id;
		}
	}
	else if (numId >= 0) // use word style default
	{
		use_numId = numId;
	}
	else if(list_state.indent_level == list_state.item_indent_level) // use internal default 1:1
	{
		if (internal_scheme_id < 0) // implement because currently not implemented
		{
			std::pair<int, pugi::xml_node> new_scheme = declareNewListScheme();
			internal_scheme_id = new_scheme.first;
			implementListScheme(new_scheme.second, internal_scheme);
		}
		use_numId = internal_scheme_id;
	}
	else // create a custom style based on internal defaults, but shifted levels
	{
		if (list_state.implemented) // an already implemented style cannot be extended. Create a new one
		{
			list_state.scheme_id = -1;
			list_state.implemented = false;
		}
		
		if (list_state.scheme_id < 0) // an empty style needs to be declared, because an ID is needed
		{
			std::pair<int, pugi::xml_node> new_scheme = declareNewListScheme();
			list_state.scheme_id = new_scheme.first;
			list_state.abstract_scheme_node = new_scheme.second;
			list_state.scheme = internal_scheme; // initialise to default, but each level should be created explicitly
			list_state.scheme.nsid = "";
			list_state.scheme.size = 0;
		}

		// initialise the levels which were not used until now
		int shift_levels = list_state.indent_level - list_state.item_indent_level;
		for (int i = list_state.scheme.size ; i <= list_state.indent_level ; ++i)
		{
			initDefaultItemizeLevel(list_state.scheme.levels[i], (i-shift_levels+9)%9, i);
		}
		list_state.scheme.size = list_state.indent_level + 1;
		use_numId = list_state.scheme_id;
	}
	
	
	
	
	// ##### from here: implement the list with its listitems
	
	// DB_Meta meta = db_meta(node); // TODO treat title, but maybe only on the first (0) level
	
	for (pugi::xml_node i : node.children())
	{
		if (i.type() != pugi::node_element) continue;
	
		const char* n = i.name();
		if (std::strcmp(n, "listitem") == 0)
		{
			BlockState sub_block_state(*list_state.block_state);
			sub_block_state.po = ParOptions(); // remove "inherited" paragraph options
			sub_block_state.po.style = style_id;
			sub_block_state.p = mkListEntryP(sub_block_state.section_state->root_state->body, use_numId, list_state.indent_level, sub_block_state.po);
// 			++list_state.indent_level;
			list_state.list_item_paragraph = sub_block_state;
			db_list_item(i, list_state);
		}
	}
	
	// ##### now implement the list scheme, if not already done
	// note that this part is reached, if the stack of levels decrease
	
	if (!list_state.implemented)
	{
		implementListScheme(list_state.abstract_scheme_node, list_state.scheme);
		list_state.implemented = true;
	}
	
	--list_state.indent_level;
	--list_state.item_indent_level;
}

void Docbook2Word::db_orderedList(pugi::xml_node node, Docbook2Word::ListState& list_state)
{
	++list_state.indent_level;
	++list_state.enum_indent_level;
	
	if (list_state.indent_level == 0)
	{

		DB_Meta meta = db_meta(node);
		if (!meta.title.empty()) mkParHeading(meta.title.c_str(), list_state.block_state->section_state->root_state->body);
		else
		{
			const char* previous = node.previous_sibling().name();
			if (strcmp(previous, "itemizedlist") == 0 || strcmp(previous, "orderedlist") == 0)
			{
				ParOptions po;
				mkPar(list_state.block_state->section_state->root_state->body, po);
			}
		}
	}
	
	/*
	 * Theree are three possible numbering "styles":
	 *    * The internal default style which is applied, if all other methods fail (internal_scheme)
	 *    * A current, user defined style, built up by user requested numbering level styles (list_state.schme), but initialised with the internal default style
	 *    * The default numbering style in the word document (style/style_id). If present it defines the font/paragraph style, but not always the numbering style.
	*/
	
	// levels > 9 (1-9 or 0-8) cannot be treated ; TODO this should cause at least a warning, because it is not allowed!
	assert(list_state.indent_level <= 8 && list_state.enum_indent_level <= 8);
	if (list_state.indent_level > 8)
	{
		// just write the content as paragraphs
		for (pugi::xml_node i : node.children())
		{
			if (i.type() != pugi::node_element) continue;
			BlockState sub_block_state(*list_state.block_state);
			sub_block_state.p = mkPar(sub_block_state.section_state->root_state->body, sub_block_state.po);
			db_nested_block_loop(i, sub_block_state);
		}
		return;
	}
	
	const bool compact = isListCompact(node, list_state.indent_level);
	const WordFile::WordStylePar &style = compact ? private_opts.enumerate_compact_style : private_opts.enumerate_normal_style;
	const std::string style_id = word_template.getParStyleID(style);
	int &numId = compact ? private_opts.enumerate_compact_numId : private_opts.enumerate_normal_numId;
	
	// use and keep an internal default scheme
	const static WordBuilder::ListScheme internal_scheme = createDefaultEnumerateScheme();
	static int internal_scheme_id = -1;
	// static bool internal_scheme_implemented = false; // not necessary, because the internal scheme has to be implemented immediately after declaration always!
	
	static int last_used_scheme_ids[9] = { -1, -1, -1, -1, -1, -1, -1, -1, -1}; // needed to be able to continue an enumeration
	
	// now get information about the user requested style
	
	// std::string mark = node.attribute("mark").as_string(); // like "*"
	
	bool inheritnum = false; // Text: for example on level 3: "%1.%2.%3" to include the lower levels too
	bool continuation = false;
	std::string numeration;
	{
		std::string inheritnum_raw = node.attribute("inheritnum").as_string(); // ignore/inherit (should the previous level "1.2" be inherited: "1.2.1"
		std::string continuation_raw = node.attribute("continuation").as_string(); // continues/restarts
		std::transform(inheritnum_raw.begin(), inheritnum_raw.end(), inheritnum_raw.begin(), ::tolower);
		std::transform(continuation_raw.begin(), continuation_raw.end(), continuation_raw.begin(), ::tolower);
		
		if (inheritnum_raw == "inherit") inheritnum = true;
		if (continuation_raw == "continues" ) continuation = true;
		
		if (!options.ignore_numeration_option)
		{
			std::string numeration_raw = node.attribute("numeration").as_string(); // arabic/loweralpha/lowerroman/upperalpha/upperroman
			std::transform(numeration_raw.begin(), numeration_raw.end(), numeration_raw.begin(), ::tolower);
			
			if (numeration_raw == "arabic") numeration = "decimal";
			else if (numeration_raw == "loweralpha") numeration = "lowerLetter";
			else if (numeration_raw == "lowerroman") numeration = "lowerRoman";
			else if (numeration_raw == "upperalpha") numeration = "upperLetter";
			else if (numeration_raw == "upperroman") numeration = "upperRoman";
		}
	}
	
	int start_with = 1;
	{
		pugi::xml_node start_info;
		start_info = node.child("dbfo");

		if (start_info) std::sscanf (start_info.value(),"start=\"%d\"",&start_with);
		else
		{
			start_info = node.child("dbhtml");
			if (start_info) std::sscanf (start_info.value(),"start=\"%d\"",&start_with);
		}
	}
	
	int use_numId = -1;
	
	bool scheme_reusable = list_state.scheme_id >= 0 && // already declared
		list_state.scheme.size > list_state.indent_level && // the level is declared too
		!list_state.scheme.levels[list_state.indent_level].is_enumeration && // not itemize
		list_state.scheme.levels[list_state.indent_level].numFmt != "bullet"; // but this should not happen, because "is_enumeration"
	
	if (continuation && last_used_scheme_ids[list_state.indent_level] != -1)
	// continue the last enumeration on same level ; has priority over all style changes (which would conflict with continuation in word, but we don't care)
	{
		use_numId = last_used_scheme_ids[list_state.indent_level];
	}
	else if (inheritnum || !numeration.empty() ) // the user requests a specific number style
	{
		if (list_state.implemented || last_used_scheme_ids[list_state.indent_level] == list_state.scheme_id) // an already implemented style cannot be extended or reused. Create a new one
		{
			list_state.scheme_id = -1;
			list_state.implemented = false;
		}
		
		if (list_state.scheme_id < 0) // an empty style needs to be declared, because an ID is needed
		{
			std::pair<int, pugi::xml_node> new_scheme = declareNewListScheme();
			list_state.scheme_id = new_scheme.first;
			list_state.abstract_scheme_node = new_scheme.second;
			list_state.scheme = internal_scheme; // initialise to default, but each level should be created explicitly
			list_state.scheme.nsid = "";
			list_state.scheme.size = 0;
		}

		// initialise the levels which were not used until now
		int shift_levels = list_state.indent_level - list_state.enum_indent_level;
		for (int i = list_state.scheme.size ; i <= list_state.indent_level ; ++i)
		{
			initDefaultEnumerateLevel(list_state.scheme.levels[i], (i-shift_levels+9)%9, i);
			// initDefaultEnumerateLevel(list_state.scheme.levels[i], i);
		}
		if (!numeration.empty())
		{
			list_state.scheme.levels[list_state.indent_level].numFmt = numeration;
			list_state.scheme.levels[list_state.indent_level].lvlText = "%" + std::to_string(list_state.indent_level + 1) + ".";
		}

		if (inheritnum && list_state.indent_level)
		{
			if (numeration.empty()) list_state.scheme.levels[list_state.indent_level].numFmt = "decimal";
			
			list_state.scheme.levels[list_state.indent_level].lvlText =
				list_state.scheme.levels[list_state.indent_level-1].lvlText + // prepend the previous level
				list_state.scheme.levels[list_state.indent_level].lvlText;
		}
		list_state.scheme.size = list_state.indent_level + 1;
		list_state.scheme.levels[list_state.indent_level].start = start_with;
		
		use_numId = list_state.scheme_id;
	}
	else if (numId >= 0) // use word style default
	{
		if (start_with != 1 || last_used_scheme_ids[list_state.indent_level] == numId)
		{
			int start_array[9] = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
			start_array[list_state.indent_level] = start_with;
			numId = copyListScheme(numId, start_array);
		}
		use_numId = numId;
	}
	else if(list_state.indent_level == list_state.item_indent_level) // use internal default 1:1
	{
		if (internal_scheme_id < 0 || last_used_scheme_ids[list_state.indent_level] == internal_scheme_id) // implement because currently not implemented, or already used
		{
			std::pair<int, pugi::xml_node> new_scheme = declareNewListScheme();
			internal_scheme_id = new_scheme.first;
			implementListScheme(new_scheme.second, internal_scheme);
		}
		use_numId = internal_scheme_id;
	}
	else // create a custom style based on internal defaults, but shifted levels
	{
		if (list_state.implemented || last_used_scheme_ids[list_state.indent_level] == list_state.scheme_id) // implement because currently not implemented, or already used
		{
			list_state.scheme_id = -1;
			list_state.implemented = false;
		}
		
		if (list_state.scheme_id < 0) // an empty style needs to be declared, because an ID is needed
		{
			std::pair<int, pugi::xml_node> new_scheme = declareNewListScheme();
			list_state.scheme_id = new_scheme.first;
			list_state.abstract_scheme_node = new_scheme.second;
			list_state.scheme = internal_scheme; // initialise to default, but each level should be created explicitly
			list_state.scheme.nsid = "";
			list_state.scheme.size = 0;
		}

		// initialise the levels which were not used until now
		int shift_levels = list_state.indent_level - list_state.enum_indent_level;
		for (int i = list_state.scheme.size ; i <= list_state.indent_level ; ++i)
		{
			initDefaultEnumerateLevel(list_state.scheme.levels[i], (i-shift_levels+9)%9, i);
			// initDefaultEnumerateLevel(list_state.scheme.levels[i], i);
		}
		list_state.scheme.size = list_state.indent_level + 1;
		list_state.scheme.levels[list_state.indent_level].start = start_with;
		use_numId = list_state.scheme_id;
	}
	
	
	
	
	// ##### from here: implement the list with its listitems
	
	// DB_Meta meta = db_meta(node); // TODO treat title, but maybe only on the first (0) level
	
	for (pugi::xml_node i : node.children())
	{
		if (i.type() != pugi::node_element) continue;
	
		const char* n = i.name();
		if (std::strcmp(n, "listitem") == 0)
		{
			BlockState sub_block_state(*list_state.block_state);
			sub_block_state.po = ParOptions(); // remove "inherited" paragraph options
			sub_block_state.po.style = style_id;
			sub_block_state.p = mkListEntryP(sub_block_state.section_state->root_state->body, use_numId, list_state.indent_level, sub_block_state.po);
			list_state.list_item_paragraph = sub_block_state;
			db_list_item(i, list_state);
		}
	}
	
	// ##### now implement the list scheme, if not already done
	// note that this part is reached, if the stack of levels decrease
	
	if (!list_state.implemented)
	{
		implementListScheme(list_state.abstract_scheme_node, list_state.scheme);
		list_state.implemented = true;
	}
	
	last_used_scheme_ids[list_state.indent_level] = use_numId;
	--list_state.indent_level;
	--list_state.enum_indent_level;
}




void Docbook2Word::db_block_screen(pugi::xml_node node, Docbook2Word::BlockState& block_state)
{
	// TODO use "Cambria Math" for "<co id="CO1-1" />"
	BlockState sub_block_state(block_state);
	sub_block_state.literally = true;
	sub_block_state.po.style = word_template.getParStyleID(WordFile::WordStylePar::S_SOURCECODE_PAR);
	sub_block_state.p = mkPar(sub_block_state.section_state->root_state->body, sub_block_state.po);
	// db_outside_inline_loop(node, sub_block_state); // simpara cannot contain block elements
	
	InlineState inline_state;
	inline_state.block_state = &sub_block_state;
	inline_state.ro.lang = options.lang;
	inline_state.ro.charstyle |= CharStyle::NO_PROOF;
	
	for (pugi::xml_node i : node.children())
	{
		// inline elements are allowed here inside of a block
		switch (i.type())
		{
			// case pugi::node_null: // Empty (null) node handle
			// case pugi::node_document: // A document tree's absolute root
			case pugi::node_element: // Element tag, i.e. '<node/>'
			{
				
				// TODO callouts here
				
				if (db_inline_select(i, inline_state)) continue;
				break;
			}
			case pugi::node_pcdata: // Plain character data, i.e. 'text'
			{
				if (db_inline_select_txt(i, inline_state)) continue;
				break;
			}
			case pugi::node_cdata: // Character data, i.e. '<![CDATA[text]]>'
			{
				if (db_inline_select_txt(i, inline_state)) continue;
				break;
			}
			// case pugi::node_comment: // Comment tag, i.e. '<!-- text -->'
			case pugi::node_pi: // Processing instruction, i.e. '<?name?>'
			{
				if (db_inline_select_pi(i, inline_state)) continue;
				break;
			}
			// case pugi::node_declaration: // Document declaration, i.e. '<?xml version="1.0"?>'
			// case pugi::node_doctype: // Document type declaration, i.e. '<!DOCTYPE doc>'
			default:
				break;
		}

	}
	
	
	
	
	
// 	for (pugi::xml_node i : node.children())
// 	{
// 		if (i.type() != pugi::node_element) continue;
// 	
// 		const char* n = i.name();
// 		if (std::strcmp(n, "listitem") == 0)
// 		{
// 			BlockState sub_block_state(*list_state.block_state);
// 			sub_block_state.po = ParOptions(); // remove "inherited" paragraph options
// 			sub_block_state.po.style = style_id;
// 			sub_block_state.p = mkListEntryP(sub_block_state.section_state.root_state.body, use_numId, list_state.indent_level, sub_block_state.po);
// // 			++list_state.indent_level;
// 			list_state.list_item_paragraph = sub_block_state;
// 			db_list_item(i, list_state);
// 		}
// 	}
	
	

		


}


void Docbook2Word::db_block_calloutlist(pugi::xml_node node, Docbook2Word::BlockState& block_state)
{

}

void Docbook2Word::db_block_figure(pugi::xml_node node, Docbook2Word::BlockState& block_state)
{
	const char* id = ""; // an optional id which may be used as anchor name
	pugi::xml_node mediaobject;
	DB_Meta meta;
	
	const char* x = node.name();
	
	if (strcmp(node.name(), "figure") == 0 || strcmp(node.name(), "informalfigure") == 0)
	{
		id = node.attribute("id").as_string();
		meta = db_meta(node);
		const char* meta_id = meta.anchor.attribute("id").as_string();
		if (meta_id && *meta_id) id = meta_id;
		mediaobject = node.child("mediaobject");
	}
	else if (strcmp(node.name(), "inlinemediaobject") == 0)
	{
		mediaobject = node;
		meta = db_meta(mediaobject);
		const char* meta_id = meta.anchor.attribute("id").as_string();
		if (meta_id && *meta_id) id = meta_id;
	}
	else return;
	
	pugi::xml_node imageobject = mediaobject.child("imageobject");
	pugi::xml_node imagedata = imageobject.child("imagedata");
	const char* fileref = imagedata.attribute("fileref").as_string();
	
// 	pugi::xml_node resulting_gfx_run; // would be needed if a bookmark to the gfx is created instead of a bookmark to the caption
	
	// #### image ####
	{
		std::string imgpath = current_db->getPath();
		// now remove the filename, to have only the path, and append the image name
		const size_t last_slash_idx = imgpath.find_last_of("\\/");
		if (std::string::npos != last_slash_idx) imgpath.erase(last_slash_idx+1);
		else imgpath.clear(); // only the file name
		imgpath += fileref;
		
		std::pair<unsigned int, unsigned int> imgsize = pngsize(imgpath.c_str());
		if (!imgsize.first || !imgsize.second) return; // TODO better error handling
		unsigned int scaling = guess_scaling(imgsize.first, imgsize.second, 15*one_cm_is, 23*one_cm_is); // TODO canvas size
		const unsigned int w_emu = scaling * imgsize.first;
		const unsigned int h_emu = scaling * imgsize.second;
		
		const std::string imgfile_name = word_template.insertMediaImageFile(imgpath.c_str());
		const std::string rid = mkImageRelationship(word_template.getRelsDoc(), std::string("media/" + imgfile_name).c_str()); // "rId11"
		if (last_picture_id == -1 )
		{
			last_picture_id = findMaxId(word_template.getDocumentBody(), "/descendant::wp:docPr", "id");
			ensureContentTypesExtension(word_template.getContentTypesDoc(), "png", "image/png");
			// ensureContentTypesExtension(word_template.getContentTypesDoc(), "tiff", "image/tiff");
		}
		++last_picture_id;
		
		// https://startbigthinksmall.wordpress.com/2010/01/04/points-inches-and-emus-measuring-units-in-office-open-xml/
		// dxa = 1/20 of a point ; 
		// 1mm = 56.69291338582677 dxa = 36000 emu
		
		BlockState img_block(block_state);
		InlineState img_inline;
		img_inline.block_state = &img_block;
		img_inline.block_state->po.keep_next = true;
		pugi::xml_node &p = img_inline.block_state->p;
		pugi::xml_node &body = img_inline.block_state->section_state->root_state->body;
		p = mkPar(body, img_inline.block_state->po);
		
		img_inline.ro.charstyle |= CharStyle::NO_PROOF;
		pugi::xml_node &r = img_inline.r;
		r = mkRun(p, img_inline.ro);
		
		pugi::xml_node inline_drawing = mkInlineDrawing(r, w_emu, h_emu, last_picture_id);
		mkGraphic(inline_drawing, w_emu, h_emu, rid.c_str(), true, 0); // 0 as id is ok here, because it is the only one in the inline container
		
// 		resulting_gfx_run = r;
	}
	
	// #### caption ####
	{
		pugi::xml_node textobject = mediaobject.child("textobject");
		BlockState sub_block_state(block_state);
		
		sub_block_state.po.style = word_template.getParStyleID(WordFile::S_CAPTION);
// 		pugi::xml_node &body = sub_block_state.section_state.root_state.body;
// 		pugi::xml_node &p = sub_block_state.p;
// 		p = mkPar(body, sub_block_state.po);
		
		// InlineState inline_state;
		// inline_state.block_state = sub_block_state;

		// pugi::xml_node &r = inline_state.r;
		// r = mkRun(p, inline_state.ro);
		
		// body = mkTxt(r, "Figure: ", false);
		// db_inside_block_loop(textobject, block_state);
		// db_inside_inline_loop(textobject, inline_state);
		
		//const char* caption = textobject.child("phrase").text().get();
		
		std::string caption = meta.title;
		if (caption.empty())
		{
			// TODO make an option switch to enable this fallback solution, or
			// mark it as deprecated to remove it later
			const std::string alt_caption = extractTextRecursive(textobject);
			if (alt_caption != fileref) caption = alt_caption;
		}
		trim(caption);
		caption = cleanText(caption.c_str(), true);
		
		// it is not possible to make a reference to an image without caption!
		// (only a link with specified text would work, because otherwise there
		// is no default text)
		if (!caption.empty())
		{
			std::string bookmark_name;
			
			// that's what word would do:
			// const std::string default_bookmark_name = std::string("Fig_" + std::to_string(last_picture_id));
			
			// AnchorProperties *ap = registerAnchor(default_bookmark_name, id);
			if (*id)
			{
				AnchorProperties *ap = registerAnchor(id);
				
				if (ap)
				{
					ap->label_by_field = true;
					ap->type = Docbook2Word::ANCHOR_FIGURE;
					bookmark_name = ap->name;
				}
			}
			
	// 		for (pugi::xml_node i : textobject.children())
	// 			for (pugi::xml_node j : i.children())
	// 				if (j.type() == pugi::xml_node_type::node_pcdata || j.type() == pugi::xml_node_type::node_cdata) caption += j.text().get();

			pugi::xml_node p = mkPar(block_state.section_state->root_state->body, sub_block_state.po);
			RunOptions ro;
			ro.lang = options.lang;
			mkFigureCaption(p, caption.c_str(), ro, bookmark_name.c_str());
		}
		
		block_state.p = pugi::xml_node();
	}
}







void Docbook2Word::db_list_item(pugi::xml_node node, ListState& list_state)
{
/*
The following elements occur in listitem: abstract, address, anchor, authorblurb, beginpage, bibliolist, blockquote,
bridgehead, calloutlist, caution, classsynopsis, cmdsynopsis, constraintdef, constructorsynopsis, destructorsynopsis,
epigraph, equation, example, fieldsynopsis, figure, formalpara, funcsynopsis, glosslist, graphic, graphicco, highlights,
important, indexterm, informalequation, informalexample, informalfigure, informaltable, itemizedlist, literallayout, mediaobject,
mediaobjectco, methodsynopsis, msgset, note, orderedlist, para, procedure, productionset, programlisting, programlistingco, qandaset,
remark, screen, screenco, screenshot, segmentedlist, sidebar, simpara, simplelist, synopsis, table, task, tip, variablelist, warning.
*/


	// TODO <listitem override='bullet'><para/></listitem> --> also <override='4'> for ordered list

	// TODO if multiple paragraphs are in one item, then separate them by a line break
	
	for (pugi::xml_node i : node.children())
	{
		if (node.type() != pugi::node_element) continue;
		
		const char* n = i.name();
		if (std::strcmp(n, "para") == 0)
		{
			db_inside_block_loop(i, list_state.list_item_paragraph);
		}
		else if (std::strcmp(n, "simpara") == 0) // simple paragraph, can only contain text but no block-level elements like figures
		{
			db_outside_inline_loop(i, list_state.list_item_paragraph); // simpara cannot contain block elements
		}
		else if (std::strcmp(n, "itemizedlist") == 0)
		{
			db_itemizedList(i, list_state);
		}
		else if (std::strcmp(n, "orderedlist") == 0)
		{
			db_orderedList(i, list_state);
		}
	}
}

void Docbook2Word::mkParHeading(const char* title, pugi::xml_node body)
{
		ParOptions po;
		// TODO use H without numbers - see also simplesect. Find appropriate style, and remove the BOLD format
		// concept: just use H1...Hx, but without numbering (local format modification)
		
		std::string heading_style = word_template.getParStyleID(WordFile::WordStylePar::S_HEADING);
		bool native_style = true;
		
		if (heading_style.empty())
		{
			heading_style = word_template.getParStyleID(WordFile::WordStylePar::S_NO_SPACE);
			po.keep_next = true;
			native_style = false;
		}
		
		po.style = heading_style;
		pugi::xml_node title_p = mkPar(body, po);
		RunOptions ro;
		if (!native_style) ro.charstyle = CharStyle::BOLD;
		ro.lang = options.lang;
		pugi::xml_node title_r = mkRun(title_p, ro);
		std::string txt = cleanText(title, true);
		mkTxt(title_r, txt.c_str(), false);
}




void Docbook2Word::mkRunStack(Docbook2Word::InlineState& inline_state)
{
	if (!inline_state.r)
	{
		if (!inline_state.block_state->p)
		{
			assert(inline_state.block_state->section_state->root_state->body);
			inline_state.block_state->p = mkPar(inline_state.block_state->section_state->root_state->body, inline_state.block_state->po);
		}
		inline_state.r = mkRun(inline_state.block_state->p, inline_state.ro);
	}
}

std::string Docbook2Word::extractTextRecursive(pugi::xml_node node)
{
	std::string result;
	
	if (node.type() == pugi::xml_node_type::node_pcdata || node.type() == pugi::xml_node_type::node_cdata) result += node.text().get();
	else
	{
		for (pugi::xml_node i : node.children()) result += extractTextRecursive(i);
	}
	return result;
}

std::string Docbook2Word::cleanText(const char* in, bool &eat_space) const
{
	bool quot_begin = eat_space;
	std::string result;
	result.reserve(strlen(in));


	// TODO -- to N-dash
	
	for (const char* i = in ; *i ; ++i)
	{
		if (*i == '"')
		{
			if (quot_begin) result += options.b_double_quotes;
			else result += options.e_double_quotes;
			quot_begin = false;
			eat_space = false;
		}
		else if (*i == '\'')
		{
			if (quot_begin) result += options.b_single_quotes;
			else result += options.e_single_quotes;
			quot_begin = false;
			eat_space = false;
		}
		else if (std::strchr(" \r\n", *i))
		{
			if (eat_space) continue;
			result += " ";
			eat_space = true;
			quot_begin = true;
		}
		else if (std::strchr("\u00A0\u202F\uFEFF\u2007\u180E", *i)) // non breaking space
		{
			result += *i;
			eat_space = true;
			quot_begin = true;
			
		}
		else if (std::strchr("[({<-", *i))
		{
			result += *i;
			eat_space = false;
			quot_begin = true;
		}
		else
		{
			result += *i;
			eat_space = false;
			quot_begin = false;
		}
	}
	
	return result;
}


pugi::xml_node Docbook2Word::next_node(pugi::xml_node current, const pugi::xml_node& root, bool children)
{
	if (children)
	{
		pugi::xml_node result = current.first_child();
		if (result) return result;
	}
	
	do
	{
		pugi::xml_node result = current.next_sibling();
		if (result) return result;
		
		current = current.parent();
	} while (current && current != root);
	
	return pugi::xml_node();
}

void Docbook2Word::logerr(const pugi::xml_node& node, const std::string& msg)
{
	std::string errmsg;
	if (msg.length()) errmsg = std::string("Error at node ") + node.name() + std::string(":") + msg + std::string("\n");
	else errmsg = std::string("Error at node ") + node.name() + std::string("\n");
	logerr(errmsg);
}


