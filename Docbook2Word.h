// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#pragma once

#include <cassert>
#include <vector>
#include <stack>
#include <map>
#include <pugixml.hpp>
#include "WordBuilder.h"
#include "DocbookFile.h"
#include "WordFile.h"



class Docbook2Word : public WordBuilder
{
public:
	
	Docbook2Word(WordFile& for_template, WordBuilder::Options& options) : WordBuilder(for_template, options) {}
	
	pugi::xml_node translate(const DocbookFile& from);
	
private:
	
	pugi::xml_document result_doc; // the result will be created beyond a <result> node, which is stored in WordBuilder::body
	
	const DocbookFile* current_db = nullptr;
	
	struct DB_Meta
	{
		std::string title;
		pugi::xml_node anchor;
	};
	DB_Meta db_meta(pugi::xml_node node);
	
	struct RootState
	{
		pugi::xml_node body;
	};
	
	struct SectionState
	{
		RootState *root_state;
		int section_level = 0;
	};
	
	struct BlockState
	{
		SectionState *section_state;
		pugi::xml_node p;
		ParOptions po;
		bool literally = false;
		bool eat_space = true; ///< Relevant for cleanText() to remember the last state. The default is true, and has to be (re-)set when a new phrase begins
		
		~BlockState();
	};
	
	struct InlineState
	{
		BlockState *block_state;
		pugi::xml_node r;
		RunOptions ro;
// 		bool r_changed = false; // if true, then a new run have to be created on return to reset the previous state
	};
	
	enum AnchorTargetType
	{
		ANCHOR_OTHER,
		ANCHOR_HEADING,
		ANCHOR_FIGURE
	};
	struct AnchorProperties
	{
		std::string name;
		std::string label; // keep empty (and do not set to a default) if not label exists - especially when label_by_field==true
		bool label_by_field = false; // for example headings: it is possible to make a field, which updates its text from the heading, because the heading txt == label
		AnchorTargetType type = ANCHOR_OTHER;
		// pugi::xml_node src; // the anchor in the docbook document
	};
	
	std::map<std::string, std::string> anchor_aliases;
	std::map<std::string, AnchorProperties> anchor_props;
	std::map<std::string, AnchorProperties> unknown_anchors_aliases;
	
	std::string cleanAnchorName(const std::string& name);
	AnchorProperties* registerAnchor(const std::string& name, const std::string& alias = std::string());
	AnchorProperties* getAnchor(const std::string name);
	
	struct BookmarkProperties
	{
		pugi::xml_node src; // the bookmark node (xref, link, ...) in the docbook document
		pugi::xml_node dst; // w:r with text (which may be a placeholder)
		// bool is_placeholder = false;
		RunOptions ro;
	};
	
	std::vector<BookmarkProperties> bookmarks_to_process;
	void process_bookmarks();
	
// 	bool need_vspace = false; ///< set to true, if the inserted element wants to be separated with vspace from the next one
	
// 	std::string last_element_type; ///< helps do determine, if seperation of similar content type is needed
	
	
	
	void db_root_loop(pugi::xml_node w_root, pugi::xml_node db_root);
	void db_outside_section_loop(pugi::xml_node node, RootState& root_state);
	void db_inside_section_loop(pugi::xml_node node, SectionState& section_state);
	void db_outside_block_loop(pugi::xml_node node, SectionState& section_state); // block level, the level where the sections are
	void db_nested_block_loop(pugi::xml_node node, BlockState& block_state);
	void db_inside_block_loop(pugi::xml_node node, BlockState& block_state);
	void db_outside_inline_loop(pugi::xml_node node, BlockState& block_state);
	void db_inside_inline_loop(pugi::xml_node node, InlineState& inline_state);
	
	bool db_section_select(pugi::xml_node node, SectionState& section_state);
	bool db_block_select_p(pugi::xml_node node, BlockState& block_state);
	bool db_block_select_misc(pugi::xml_node node, BlockState& block_state);
	
	bool db_inline_select(pugi::xml_node node, InlineState& inline_state);
	bool db_inline_select_txt(pugi::xml_node node, InlineState& inline_state);
	bool db_inline_select_pi(pugi::xml_node node, InlineState& inline_state);
	
	std::map<std::string, int> footnotes; // docbook ID -> word ID
	bool db_inline_footnote(pugi::xml_node node, InlineState& inline_state, pugi::xml_node last_wr = pugi::xml_node());
	


	struct ListState
	{
		BlockState* block_state = NULL; // parent
		BlockState list_item_paragraph;
		
		WordBuilder::ListScheme scheme;
		int enum_indent_level = -1; // if the default was used
		int item_indent_level = -1; // if the default was used
		int indent_level = -1; // the real ident level independent of which style was used
		int scheme_id = -1; // indicate, that the scheme is defined (with that id) and used
		bool implemented = false; // true if implemented (because it may be defined and used, but not implemented)
		pugi::xml_node abstract_scheme_node;
	};
	
	bool isListCompact(pugi::xml_node node, int level) const;
	void db_block_itemizedList(pugi::xml_node node, BlockState& block_state);
	void db_block_orderedList(pugi::xml_node node, BlockState& block_state);
	void db_itemizedList(pugi::xml_node node, ListState& list_state);
	void db_orderedList(pugi::xml_node node, ListState& list_state);
	void db_list_item(pugi::xml_node node, ListState& list_state);
	
	void db_block_screen(pugi::xml_node node, BlockState& block_state);
	void db_block_calloutlist(pugi::xml_node node, BlockState& block_state);
	
	void db_block_figure(pugi::xml_node node, BlockState& block_state);
	
	
	void mkParHeading(const char* title, pugi::xml_node body);
	
	static void mkRunStack(InlineState& inline_state);
	
	static std::string extractTextRecursive(pugi::xml_node node);
	
	std::string cleanText(const char* in, bool& eat_space) const;
	
	inline std::string cleanText(const char* in, bool&& eat_space) const
	{
		return cleanText(in, eat_space);
	}
	
	std::vector<std::string> errlog;
	inline void logerr(const std::string& errmsg) { errlog.push_back(errmsg); }
	void logerr(const pugi::xml_node& node, const std::string& msg = "");
	
	pugi::xml_node next_node(pugi::xml_node current, const pugi::xml_node& root, bool children = true);
	int last_picture_id = -1;
};

