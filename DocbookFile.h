// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#pragma once

#include <pugixml.hpp>
#include <string>

class DocbookFile
{
private:
	pugi::xml_document xml_document;
	std::string path;
public:
	bool open(const std::string& path);
	inline const pugi::xml_document& getData() const { return xml_document; }
	inline const std::string& getPath() const { return path; }
};

