// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#pragma once

#include <pugixml.hpp>
#include <string>
#include "WordFile.h"

class WordBuilder
{
public:
	struct Options
	{
		bool empty_paragraph_separator = false; // use empty paragraphs between paragraphs for separation
		int section_offset = 0;
		std::string lang;
		bool ignore_numeration_option = true; // asciidoc always produces enumeration style definitions, even when the default is wanted
		bool rm_lead_footnote_blank = true; // asciidoc produces a blank before <footnote>
		// std::string namespace_prefix; // used to ensure distinct IDs (for example hyperlink IDs) if many sources are merged into the same destination
		
		std::string b_double_quotes = "\"";
		std::string e_double_quotes = "\"";
		
		std::string b_single_quotes = "'";
		std::string e_single_quotes = "'";
		
		// https://startbigthinksmall.wordpress.com/2010/01/04/points-inches-and-emus-measuring-units-in-office-open-xml/
		// dxa = 1/20 of a point ; 
		// 1mm = 56.69291338582677 dxa = 36000 emu
		int canvas_width_emu;
		int canvas_height_emu;
	};
	
	WordBuilder(WordFile& for_template, Options& options);
	
	void setOptionsForField(const WordFile::Field& field);
	
	enum CharStyle
	{
		PLAIN = 0b0,
		ITALIC = 0b1,
		BOLD = 0b10,
		UNDERLINE = 0b100,
		STRIKE = 0b1000,
		SUPER = 0b10000,
		SUB = 0b100000,
		NO_PROOF = 0b1000000,
		HIDDEN = 0b10000000
	};
	
	struct ParOptions
	{
		ParOptions() {}
		std::string style;
		
		int ind_left = 0; // twips
		int ind_right = 0; // twips
		int ind_hanging = 0; // twips
		int ind_first_line = 0; // twips
		
		bool keep_next = false;
	};
	
	struct FontOptions
	{
		std::string ascii; // font for the ASCII range (U+0000-U+007F)
		std::string hAnsi; // font for any other unicode range (high ANSI)
		std::string cs; // font for complex script Unicode range
		std::string hint; // Font Content Type (default -> High ANSI Font)
	};
	
	struct RunOptions
	{
		RunOptions() {}
		
		int charstyle = PLAIN;
		std::string lang;
		std::string style;
		std::string colour; // "auto"
		std::string highlight;
		
		FontOptions font;
		
		int spacing = 0; // 1/20 point
		int kern = 0; // 1/2 point
		int sz = 0; // Font Size (1/2)
		int szCs = 0; // Complex Script Font Size (1/2 point)
	};
	
	typedef std::pair<pugi::xml_node, pugi::xml_node> XMLPair;

	static pugi::xml_node mkpPr(pugi::xml_node parent, const ParOptions& options = {}, bool enforce = false);
	static pugi::xml_node mkPar(pugi::xml_node parent, const ParOptions& options = {});
	static pugi::xml_node mkrPr(pugi::xml_node parent, const RunOptions& options = {}, bool enforce = false);
	static pugi::xml_node mkRun(pugi::xml_node parent, const RunOptions& options = {});
	static pugi::xml_node mkRunBefore(pugi::xml_node before, const RunOptions& options = {});
	static pugi::xml_node mkRunAfter(pugi::xml_node after, const RunOptions& options = {});
	
	
	static pugi::xml_node mkTxt(pugi::xml_node parent, const char* text, bool as_cdata, bool preserve_space, bool force_create_new);
	static pugi::xml_node mkTxt(pugi::xml_node parent, const char* text, bool as_cdata);
	static pugi::xml_node mkData(pugi::xml_node parent, const char* txtdata, bool as_cdata = false);
	pugi::xml_node mkHeading(pugi::xml_node parent, const ParOptions& options, const char* text = nullptr, const char* bookmark_name = nullptr);
	pugi::xml_node mkHeading(pugi::xml_node parent, int level, const char* text = nullptr, const char* bookmark_name = nullptr);
	
	pugi::xml_node mkListEntryP(pugi::xml_node parent, int numId, int level = 0, const WordBuilder::ParOptions& options = {}, bool make_numId_copy = false);
	
	
	void mkBookmark(pugi::xml_node from_node, pugi::xml_node to_node, const char* name);
	static std::pair<pugi::xml_node, pugi::xml_node> mkField(pugi::xml_node from_code, pugi::xml_node to_code, pugi::xml_node to_content, const WordBuilder::RunOptions& ro, bool lock, bool mergeformat);
	static std::pair<pugi::xml_node, pugi::xml_node> mkField(pugi::xml_node from_node, pugi::xml_node to_node, const std::string& code, const WordBuilder::RunOptions& ro, bool lock, bool mergeformat);
	static void mkBookmarkRef(pugi::xml_node from_node, pugi::xml_node to_node, const char* name, const char* typestr, WordBuilder::RunOptions ro);
	std::pair<pugi::xml_node, pugi::xml_node> mkFigureCaption(pugi::xml_node p, const char* caption, const RunOptions& ro, const char* bookmark_name = "");
	
	static pugi::xml_node mkFootnoteP(pugi::xml_document& footnote_doc, int id, const std::string& p_style, const WordBuilder::RunOptions& ref_nr_format);
	void mkFootnoteNr(pugi::xml_node p, int id, const WordBuilder::RunOptions& ro, const char* bookmark_name = NULL);
	static void mkFootnoteRefNr(pugi::xml_node p, const char* bookmark_name, const char* txt_content, const WordBuilder::RunOptions& ro);
	
	std::string mkMediaRelationship(pugi::xml_document& rel_doc, const char* type, const char* target, const char* target_mode = "");
	std::string mkImageRelationship(pugi::xml_document& rel_doc, const char* target);
	static pugi::xml_node mkInlineDrawing(pugi::xml_node r, unsigned int widht_emu, unsigned int height_emu, int id);
	static pugi::xml_node mkGraphic(pugi::xml_node wp_container, unsigned int widht_emu, unsigned int height_emu, const char* blipId, bool embed, int id = 0);

	
	static void appendDefaultNamespaceAttribs(pugi::xml_node node);
	static void ensureContentTypesExtension(pugi::xml_document& content_types_doc, const char* extension, const char* content_type);
	
	struct ListLevel
	{
		bool is_enumeration = false; // if it is an "ordered list"
		int start = 1;
		std::string numFmt = "bullet";
		std::string lvlText = "\xEF\x82\xB7";
		std::string lvlJc = "left";
		std::string pStyle;
		ParOptions par_options; // as declared in the num scheme, not the options in the document!
		RunOptions run_options; // as declared in the num scheme, not the options in the document!
	};
	
	struct ListScheme
	{
		std::string nsid; // very optional
		std::string multiLevelType = "singleLevel"; // just a hint for the GUI: singleLevel/multiLevel/hybridMultiLevel <w:multiLevelType w:val="hybridMultilevel" />
		int size = 0; // the current size/number of levels, counted from 0 to 9, because 0 if empty
		ListLevel levels[9]; // but this is an array, counted from 0 to 8, which are 9 slots
	};
	

	std::pair<int, pugi::xml_node> declareNewListScheme();
	void implementListScheme(pugi::xml_node abstractNum_node, const ListScheme& scheme);
	static void initDefaultEnumerateLevel(WordBuilder::ListLevel& that, int level, int indent_level = -1);
	static void initDefaultItemizeLevel(WordBuilder::ListLevel& that, int level, int indent_level = -1);
	static WordBuilder::ListScheme createDefaultEnumerateScheme();
	static WordBuilder::ListScheme createDefaultItemizeScheme();
	int copyListScheme(int numId, const int start_counter_array[] = nullptr);
	


	
	int findMaxId(const pugi::xpath_node& node, const char* query, const char* attribute, int start_id = 0);
	
	static void ltrim(std::string& str);
	static void rtrim(std::string& str);
	static void trim(std::string& str);
	
protected:
	Options &options;
	WordFile &word_template;
	// pugi::xml_node body; // the result which have to be created, and can be attached to another document from a sub class
	
	struct PrivateOptions
	{
		int next_bookmark_id = 0;
		int next_footnote_id = 1; // -1 and 0 are reserved
		
		WordFile::WordStylePar itemize_normal_style = WordFile::S_INVALID_PAR_STYLE;
		WordFile::WordStylePar itemize_compact_style = WordFile::S_INVALID_PAR_STYLE;
		WordFile::WordStylePar enumerate_normal_style = WordFile::S_INVALID_PAR_STYLE;
		WordFile::WordStylePar enumerate_compact_style = WordFile::S_INVALID_PAR_STYLE;
		int itemize_normal_numId = -1;
		int itemize_compact_numId = -1;
		int enumerate_normal_numId = -1;
		int enumerate_compact_numId = -1;
	};
	PrivateOptions private_opts;
	
	
};
