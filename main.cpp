// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#include <iostream>

#include <string>
#include <optionparser.h>
#include "WordFile.h"
#include "DocbookFile.h"
#include "Docbook2Word.h"
#include "DocConfig.h"



struct Arg: public option::Arg
{
private:
	static void printError(const char* msg1, const option::Option& opt, const char* msg2)
	{
		fprintf(stderr, "ERROR: %s", msg1);
		fwrite(opt.name, opt.namelen, 1, stderr);
		fprintf(stderr, "%s", msg2);
	}

public:
	
	// return:
	//   ARG_NONE     The option does not take an argument (the argument isn't collected by that option)
	//   ARG_OK       The argument is acceptable for the option (the argument is collected by that option)
	//   ARG_IGNORE   The argument is not acceptable but that's non-fatal because the option's argument is optional
	//   ARG_ILLEGAL  The argument is not acceptable and that's fatal.
	// ARG_NONE and ARG_ILLEGAL are treated identically
	
	// Inherited:
	// None: For options that don't take an argument, but never throws an error. Just returns ARG_NONE.
	// Optional: Returns ARG_OK if the argument is attached and ARG_IGNORE otherwise.
	
	// print an error if that option is used
	static option::ArgStatus Unknown(const option::Option& option, bool msg)
	{
		if (msg) printError("Unknown option '", option, "'\n");
		return option::ARG_ILLEGAL;
	}
	
	// the option must not have an argument
	static option::ArgStatus Prohibited(const option::Option& option, bool msg)
	{
		// if (option.arg && option.name[option.namelen] != 0) return option::ARG_OK;
		
		// note: the check option.name[]==0 is needed to exclude arguments without = after
		// the option, which are not treated as option argument here, because it is a non-option
		// argument (also called "positional argument")
		if (!option.arg || option.name[option.namelen] == 0) return option::ARG_NONE;
		if (msg) printError("Option '", option, "' does not accept an argument\n");
		return option::ARG_ILLEGAL;
	}
	
	static option::ArgStatus Required(const option::Option& option, bool msg)
	{
		if (option.arg != 0) return option::ARG_OK;
		if (msg) printError("Option '", option, "' requires an argument\n");
		return option::ARG_ILLEGAL;
	}
	static option::ArgStatus NonEmpty(const option::Option& option, bool msg)
	{
		if (option.arg != 0 && option.arg[0] != 0) return option::ARG_OK;
		if (msg) printError("Option '", option, "' requires a non-empty argument\n");
		return option::ARG_ILLEGAL;
	}
	static option::ArgStatus Numeric(const option::Option& option, bool msg)
	{
		char* endptr = 0;
		if (option.arg != 0 && strtol(option.arg, &endptr, 10)){};
		if (endptr != option.arg && *endptr == 0) return option::ARG_OK;
		if (msg) printError("Option '", option, "' requires a numeric argument\n");
		return option::ARG_ILLEGAL;
	}
};


int main(int argc, char **argv)
{
	std::string arg0;
	std::string arg0_path;
	std::string arg0_file("docxtk");
	
	if (argc)
	{
		std::string arg0(argv[0]);
		std::size_t found = arg0.find_last_of("/\\");
		if (found !=  std::string::npos)
		{
			arg0_path = arg0.substr(0,found);
			arg0_file = arg0.substr(found+1);
		}
		else arg0_file = arg0;
		
		--argc;
		++argv;
	}
	
	enum OptIdx : unsigned int
	{
		UNKNOWN = 0, // for dummy descriptors: useful for text which does not belong to an option
		HELP,
		VERSION,
		SRCTYPE,
		META,
		OVERWRITE,
		MARK,
		INSERTION,
		LANG,
		PRETTY
	};
	
	enum OptType : unsigned int
	{
		UNSPECIFIED = 0,
		ENABLE = 1,
		
		// for SRCTYPE
		DOCBOOK,
		WORD,
		TEXTFILE,
		STRING,
		
		// for MARK
		FIELD,
		PLACEHOLDER,
		
		// for INSERTION
		REPLACE,
		APPEND,
	};
	
	const std::string help_title = "USAGE: " + arg0_file + " [options] [src] [dst]";
	
	// note: for options like --enable-x and --disable-x just use the same index, but different types
	const option::Descriptor cmdusage[] =
	{
		// index, type, short, long, check_arg option::Arg::None|Optional|userdefined, help
		{OptIdx::UNKNOWN, 0, "" , "", Arg::Unknown, help_title.c_str() }, // the first dummy descriptor IS used to collect unknown options (necessary!)
		{OptIdx::UNKNOWN, 0, "" , "", Arg::Unknown, "  [src]   \tThe docx file as a template containing at least one field" },
		{OptIdx::UNKNOWN, 0, "" , "", Arg::Unknown, "  [dst]   \tThe resulting docx file to be written" },
		{OptIdx::UNKNOWN, 0, "" , "", Arg::Unknown, "" },
		{OptIdx::UNKNOWN, 0, "" , "", Arg::Unknown, "Options:" },
		
		{OptIdx::HELP, 0, "h" , "help", Arg::Prohibited, "  -h,--help  \tDisplay this help and exit." },
		{OptIdx::VERSION, 0, "" , "version", Arg::Prohibited, "     --version  \tDisplay version information and exit." },
		
		{OptIdx::SRCTYPE, OptType::DOCBOOK, "d" , "docbook", Arg::NonEmpty, "  -d,--docbook [file]  \tGet the content from that docbook file" },
//		{OptIdx::SRCTYPE, OptType::WORD, "w" , "word", Arg::NonEmpty, "  -w,--word [file]  \tGet the content from that word file" },
//		{OptIdx::SRCTYPE, OptType::TEXTFILE, "t" , "text", Arg::NonEmpty, "  -t,--text [file]  \tGet the content from that text file" },
//		{OptIdx::SRCTYPE, OptType::STRING, "c" , "content", Arg::Required, "  -c,--content [text]  \tUse this text as content" },
//		{OptIdx::META, 0, "m" , "meta", Arg::NonEmpty, "  -m,--meta [key]=[val]  \tManipulate meta data" },
		{OptIdx::OVERWRITE, OptType::ENABLE, "O" , "overwrite", Arg::Prohibited, "  -O,--overwrite  \tOverwrite [src] with the result (omit [dst])" },
		{OptIdx::MARK, OptType::FIELD, "f" , "field", Arg::NonEmpty, "  -f,--field [name]  \tInsert the content at the place of this field, defaults to body" },
//		{OptIdx::MARK, OptType::PLACEHOLDER, "p" , "placeholder", Arg::NonEmpty, "  -p,--placeholder [text]  \tSearch this text (instead of a field) and place the content there" },
//		{OptIdx::INSERTION, OptType::REPLACE, "r" , "replace", Arg::Prohibited, "  -r,--replace  \tReplace the found mark (field or text) by the content (this is the default)" },
//		{OptIdx::INSERTION, OptType::APPEND, "a" , "append", Arg::Prohibited, "  -a,--append  \tKeep the found mark (field or text) like a cursor, and place the content in front of it" },
		
		{OptIdx::LANG, 0, "l" , "language", Arg::Required, "  -l,--language [lang]  \tSpecify the language like de-DE, en-GB or en-US; defaults to en-US; other than de-DE or en-US are not supported (regarding for example quotation marks)" },
		// {OptIdx::LANG, 0, "l" , "language", Arg::Required, "  -l,--language [lang]  \tSpecify the language like de-DE, en-GB or en-US; empty string: default/detect" },
		
		{OptIdx::PRETTY, 0, "P" , "pretty", Arg::Prohibited, "  -p,--pretty  \tWrite pretty XML (with indentation) to facilitate debugging of the word file" },
		
		{0,0,0,0,0,0} // terminator
	};
	
	// argc-=(argc>0); argv+=(argc>0); // skip program name argv[0] if present
	option::Stats cmdstats(true, cmdusage, argc, argv); // note: "true" enables GNU behaviour, and disableds POSIX
	option::Option cmdopts[cmdstats.options_max];
	option::Option buffer[cmdstats.buffer_max];
	option::Parser optparser(true, cmdusage, argc, argv, cmdopts, buffer); // note: "true" enables GNU behaviour, and disableds POSIX
	
	for (option::Option* opt = cmdopts[UNKNOWN]; opt; opt = opt->next())
		std::cerr << "Unknown option: " << opt->name << "\n";
	
	if (optparser.error())
	{
		std::cerr << "Exited with Error(s). Try \"" << arg0_file << " --help\" for more information.\n";
		return 1;
	}
	if (cmdopts[OptIdx::HELP] || argc == 0)
	{
		option::printUsage(std::cout, cmdusage);
		return 0;
	}
	if (cmdopts[OptIdx::VERSION] || argc == 0)
	{
		std::cerr << arg0_file << " Version 1.0.0, Copyright (c) Christoph Kloos 2018\n";
		std::cerr << "This tool is subject to the terms and conditions of the\n";
		std::cerr << "GNU GPL Version 3.0 | http://www.gnu.org/licenses/\n";
		std::cerr << "SPDX-License-Identifier: GPL-3.0\n";
		return 0;
	}
	
	if (!cmdopts[OptIdx::SRCTYPE])
	{
		std::cerr << "Missing content source option(s).\n";
		std::cerr << "Exited with Error(s). Try \"" << arg0_file << " --help\" for more information.\n";
		return 1;
	}
	
// 	for (unsigned int i = 0; i < optparser.optionsCount(); ++i)
// 	{
// 		option::Option& opt = buffer[i];
// 	}
	
	struct AppOpts
	{
		bool overwrite = false;
		std::string fieldname = "body";
	};
	AppOpts appopts;
	
	if (cmdopts[OptIdx::OVERWRITE] && cmdopts[OptIdx::OVERWRITE].last()->type() == OptType::ENABLE)
		appopts.overwrite = true;
	
	
	if ( (appopts.overwrite && optparser.nonOptionsCount() < 1) || (!appopts.overwrite && optparser.nonOptionsCount() < 2) )
	{
		std::cerr << "Missing arguments [src] [dst].\n";
		std::cerr << "This also may be caused by an option with missing argument.\n";
		std::cerr << "Exited with Error(s). Try \"" << arg0_file << " --help\" for more information.\n";
		return 1;
	}
	
	if (appopts.overwrite && optparser.nonOptionsCount() > 1)
	{
		std::cerr << "Too many arguments.\n";
		std::cerr << "The overwrite option was given (to overwrite the source template file),\n";
		std::cerr << "but also a destination file. These cannot be combined.\n";
		std::cerr << "Exited with Error(s). Try \"" << arg0_file << " --help\" for more information.\n";
		return 1;
	}
	
	if (!appopts.overwrite && optparser.nonOptionsCount() > 2)
	{
		std::cerr << "Too many arguments.\n";
		std::cerr << "Exited with Error(s). Try \"" << arg0_file << " --help\" for more information.\n";
		return 1;
	}
	

	
	
	const char* src = optparser.nonOption(0);
	const char* dst = appopts.overwrite ? src : optparser.nonOption(1);
	
	WordFile w;
	if (! w.open(src))
	{
		std::cerr << "Could not open " << src << ".\n";
		std::cerr << "Exited with Error(s).\n";
		return 2;
	}
	DocConfig::syncWordTemplateConfig(std::string(src) + ".config", w);
	WordBuilder::Options wb_options;
	wb_options.lang = "en-US";
	
	for (option::Option* i = buffer; i < buffer+optparser.optionsCount(); ++i)
	{
		switch (i->index())
		{
			case (SRCTYPE):
				if (i->type() == OptType::DOCBOOK)
				{
					DocbookFile d;
					if (d.open(i->arg))
					{
						// note that WordFile::fieldAssignContent(string fieldname) is not used here,
						// because every field may have its own options
						for (WordFile::Field* i : w.fieldsByName(appopts.fieldname))
						{
							Docbook2Word d2w(w, wb_options);
							d2w.setOptionsForField(*i);
							pugi::xml_node result = d2w.translate(d);
							w.fieldAssignContent(*i, result, true);
						}
					}
					else
					{
						std::cerr << "Could not open " << i->arg << ".\n";
						std::cerr << "Exited with Error(s).\n";
						return 2;
					}
				}
				else if (i->type() == OptType::STRING)
				{
					if (!w.fieldAssignString(appopts.fieldname, i->arg))
					{
						std::cerr << "Warning: Field " << appopts.fieldname << " was not found!\n";
					}
				}
				else if (i->type() == OptType::TEXTFILE)
				{
					std::string txt;
					
					fopen(i->arg, "r");
					// TODO missing
					
					if (!w.fieldAssignString(appopts.fieldname, txt))
					{
						std::cerr << "Warning: Field " << appopts.fieldname << " was not found!\n";
					}
				}
				break;
			case (MARK):
				appopts.fieldname = i->arg;
				break;
			case (LANG):
				wb_options.lang = i->arg;
				break;
		}
	}
	
	if (!w.saveAs(dst, cmdopts[OptIdx::PRETTY]))
	{
		std::cerr << "Could not write " << dst << ".\n";
		std::cerr << "Exited with Error(s).\n";
		return 2;
	}
	
	return 0;
}
