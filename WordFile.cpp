// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#include <fstream>
#include <sstream> // stringbuf
#include <regex>
#include <cstring>
#include <cstdlib>
#include <cassert>
#include <methods/DeflateMethod.h>
#include "WordFile.h"
#include "XMLHelper.h"
#include <pugixml.hpp>

#include <iostream>
#include <cstdio>

bool WordFile::load_xml_doc(pugi::xml_document& xml_doc, const std::string& entry_name)
{
	ZipArchiveEntry::Ptr zip_entry;
	zip_entry = wordzip->GetEntry(entry_name);
	if (!zip_entry) return false;
	std::istream* io = zip_entry->GetDecompressionStream();
	if (!io) return false;
	xml_doc.load(*io, pugi::parse_default | pugi::parse_ws_pcdata_single);
	zip_entry->CloseDecompressionStream();
	if (!xml_doc) return false;
	return true;
}

bool WordFile::save_xml_doc(pugi::xml_document& xml_doc, const std::string& entry, bool pretty)
{
	// It is not an error if there is just no XML data to save. There may be files which MUST exist in the zip file,
	// but these files also must not be empty
	if (!xml_doc.document_element()) return true; // ignore empty XML files
	
	std::stringstream io;
	
	if (pretty) xml_doc.save(io); // TODO
	else xml_doc.save(io, "", pugi::format_raw, pugi::xml_encoding::encoding_utf8); // TODO
	
	ZipArchiveEntry::Ptr zip_entry = wordzip->GetEntry(entry);
	
	if (zip_entry)
	{
		// Warning! Do not use zip_entry->Remove() because it will cause errors which are difficult to debug!
		// (The shared pointer does not know that the object is already deleted, and want to delete it again)
		zip_entry.reset(); // this deletes the object
		// now it can be removed
		wordzip->RemoveEntry(entry);
	}
	
	zip_entry = wordzip->CreateEntry(entry);
	if (!zip_entry) return false;
	
	zip_entry->SetCompressionStream(io, DeflateMethod::Create(), ZipArchiveEntry::CompressionMode::Immediate);
	io.clear();
	return true;
}



bool WordFile::open(const std::string& path)
{
	std::ifstream* zipfile_io = new std::ifstream();
	zipfile_io->open(path, std::ios::binary | std::ios::in);
	if (!zipfile_io->is_open()) return false;
	wordzip = ZipArchive::Create(zipfile_io, true);
	if (!wordzip) return false;
	
	if (!load_xml_doc(xml_document, "word/document.xml")) return false;
	if (!load_xml_doc(xml_styles, "word/styles.xml")) return false;
	load_xml_doc(xml_numbering, "word/numbering.xml");
	load_xml_doc(xml_footnotes, "word/footnotes.xml");
	load_xml_doc(xml_rels, "word/_rels/document.xml.rels");
	load_xml_doc(xml_content_types, "[Content_Types].xml");
	
	populateStyles();
	findAllFields();
	
	return true;
}

bool WordFile::saveAs(const std::string& path, bool pretty)
{
	std::ofstream zipfile_io;
	zipfile_io.open(path, std::ios::binary | std::ios::trunc);
	if (!zipfile_io) return false;
	
	try {
		if (!save_xml_doc(xml_document, "word/document.xml", pretty)) return false;
		// if (!save_xml_doc(xml_styles, "word/styles.xml", pretty)) return false;
		save_xml_doc(xml_numbering, "word/numbering.xml", pretty);
		save_xml_doc(xml_footnotes, "word/footnotes.xml", pretty);
		save_xml_doc(xml_rels, "word/_rels/document.xml.rels", pretty);
		save_xml_doc(xml_content_types, "[Content_Types].xml", pretty);
		
		assert(wordzip);
		
		wordzip->WriteToStream(zipfile_io);
	}
	
	catch(...)
	{
		zipfile_io.close();
		return false;
	}
	
	zipfile_io.close();
	return true;
}

pugi::xml_node WordFile::getDocumentBody()
{
	return xml_document.child("w:document").child("w:body");
}

pugi::xml_document& WordFile::getFootnoteDoc()
{
	return xml_footnotes;
}

pugi::xml_document& WordFile::getNumberingDoc()
{
	return xml_numbering;
}

pugi::xml_document& WordFile::getRelsDoc()
{
	return xml_rels;
}

pugi::xml_document& WordFile::getContentTypesDoc()
{
	return xml_content_types;
}

std::vector< WordFile::Field* > WordFile::fieldsByName(const std::string name)
{
	std::vector<Field*> result;
	auto r = fields.equal_range(name);
	auto i = r.first;
	while (i != r.second)
	{
		Field &field = i->second;
		assert(field.name == name); // avoid errors based on misuse of multimap and/or iterator stuff
		result.push_back(static_cast<Field*>(&field));
		++i;
	}
	return result;
}


bool WordFile::fieldAssignContent(WordFile::Field& field, const pugi::xml_node with_that_content, bool replace)
{
	switch (field.type)
	{
		case F_SIMPLE_PARAGRAPH:
		case F_COMPLEX_PARAGRAPH:
		{
			pugi::xml_node parent = field.node_paragraph.parent();
			pugi::xml_node after_that = field.node_paragraph;
			for (const auto& i : with_that_content.children())
			{
				// filter by field type - word does not like invalid references at the wrong place, etc.
				if(
					std::strcmp(i.name(), "w:p") == 0 ||
					std::strcmp(i.name(), "w:tbl") == 0
				)
					after_that = parent.insert_copy_after(i, after_that);
			}
			
			/*
				or just filter out these:
				w:sectPr
					w:headerReference
			*/
			
			const bool done = field.node_paragraph.parent().remove_child(field.node_paragraph);
			assert(done);
			
			return true;
		}
		break;
		
		default:
			return false;
		break;
	}
	return false;
}

bool WordFile::fieldAssignString(WordFile::Field& field, const std::string& with_this_string, bool replace)
{
	switch (field.type)
	{
		case F_SIMPLE_PARAGRAPH:
		{
			pugi::xml_node prev_p_style = field.node_paragraph.child("w:pPr");
			pugi::xml_node prev_r_style = field.node_start.child("w:r").child("w:rPr");
			
			pugi::xml_node new_par = field.node_paragraph.parent().insert_child_after("w:p", field.node_paragraph);
			if (prev_p_style) new_par.append_copy(prev_p_style);
			pugi::xml_node new_run = new_par.append_child("w:r");
			if (prev_r_style) new_run.append_copy(prev_r_style);
			pugi::xml_node new_txt = new_run.append_child("w:t");
			new_txt.text().set(with_this_string.c_str());

			// delete the old node
			const bool done = field.node_paragraph.parent().remove_child(field.node_paragraph);
			assert(done);
			return true;
		}
		break;
		
		case F_SIMPLE_TXT:
		{
			pugi::xml_node prev_r_style = field.node_start.child("w:r").child("w:rPr");
			
			pugi::xml_node new_run = field.node_start.parent().insert_child_after("w:r", field.node_start);
			if (prev_r_style) new_run.append_copy(prev_r_style);
			pugi::xml_node new_txt = new_run.append_child("w:t");
			new_txt.text().set(with_this_string.c_str());
			
			// delete the old node
			const bool done = field.node_start.parent().remove_child(field.node_start);
			assert(done);
			return true;
		}
		break;
		
		case F_COMPLEX_PARAGRAPH:
		{
			pugi::xml_node prev_p_style = field.node_paragraph.child("w:pPr");
			pugi::xml_node prev_r_style = field.node_start.child("w:rPr"); // note the difference: node_start IS the <w:r> tag
			
			pugi::xml_node new_par = field.node_paragraph.parent().insert_child_after("w:p", field.node_paragraph);
			if (prev_p_style) new_par.append_copy(prev_p_style);
			pugi::xml_node new_run = new_par.append_child("w:r");
			if (prev_r_style) new_run.append_copy(prev_r_style);
			pugi::xml_node new_txt = new_run.append_child("w:t");
			new_txt.text().set(with_this_string.c_str());
			
			// delete the old node
			const bool done = field.node_paragraph.parent().remove_child(field.node_paragraph);
			assert(done);
			return true;
		}
		break;
		
		case F_COMPLEX_TXT:
		{
			pugi::xml_node prev_r_style = field.node_start.child("w:rPr"); // note the difference: node_start IS the <w:r> tag
			
			pugi::xml_node new_run = field.node_end.parent().insert_child_after("w:r", field.node_end); // difference: node_end
			if (prev_r_style) new_run.append_copy(prev_r_style);
			pugi::xml_node new_txt = new_run.append_child("w:t");
			new_txt.text().set(with_this_string.c_str());
			
			// delete the old nodes
			pugi::xml_node current = field.node_start;
			pugi::xml_node parent = current.parent();
			assert(current);
			while (current != field.node_end)
			{
				pugi::xml_node next = current. next_sibling();
				const bool done = parent.remove_child(current);
				assert(done);
				current = next;
				assert(current);
			}
			const bool done = parent.remove_child(current);
			assert(done);
			return true;
		}
		break;
		
		default:
			return false;
		break;
	}
	
	
	return false;
}


unsigned int WordFile::fieldAssignContent(const std::string& this_field, const pugi::xml_node with_that_content, bool replace)
{
	unsigned int result = 0;
	
	// not anymore, because a parent (like <w:body>) with many <w:p> is passed, not a single <w:p>
	// if (std::strcmp(by_that_paragraph.name(), "w:p") != 0) return result;
// 	assert(std::strcmp(with_that_content.name(), "w:p") == 0);
	
	auto r = fields.equal_range(this_field);
	auto i = r.first;
	while (i != r.second)
	{
		Field &field = i->second;
		assert(field.name == this_field); // avoid errors based on misuse of multimap and/or iterator stuff
		if (fieldAssignContent(field, with_that_content, replace))
		{
			++result;
			i = fields.erase(i);
		}
		else ++i;
		
	}
	return result;
}

unsigned int WordFile::fieldAssignString(const std::string& this_field, const std::string& with_this_string, bool replace)
{
	unsigned int result = 0;

	auto r = fields.equal_range(this_field); // get a pair with begin (first) and end (second)
	auto i = r.first;
	while (i != r.second)
	{
		Field &field = i->second;
		assert(field.name == this_field); // avoid errors based on misuse of multimap and/or iterator stuff
		if (fieldAssignString(field, with_this_string, replace))
		{
			++result;
			i = fields.erase(i);
		}
		else ++i;
	}
	return result;
}




void WordFile::populateStyles()
{
	const std::string ptype_ = std::string("paragraph");
	const std::string ctype_ = std::string("character");
	
	pugi::xml_node styles_root = xml_styles.child("w:styles");
	
	// map from "heading 1" of <w:name w:val="heading 1" />
	// to "berschrift1" of <w:style w:type="paragraph" w:styleId="berschrift1">
	
	for (pugi::xml_node xstyle = styles_root.child("w:style"); xstyle; xstyle = xstyle.next_sibling("w:style"))
	{
		pugi::xml_node xstylename = xstyle.child("w:name");
		if (xstylename)
		{
			const std::string styletype = xstyle.attribute("w:type").as_string(); // paragraph or character
			
			const std::string stylename = xstylename.attribute("w:val").as_string();
			if (stylename.length())
			{
				const std::string id = xstyle.attribute("w:styleId").as_string();
				if (id.length())
				{
					if (styletype == ptype_)
					{
						par_style_name2id.insert(std::make_pair(stylename, id));
						par_style_id2name.insert(std::make_pair(id, stylename));
					}
					else if (styletype == ctype_)
					{
						txt_style_name2id.insert(std::make_pair(stylename, id));
						txt_style_id2name.insert(std::make_pair(id, stylename));
					}
				}
			}
		}
	}
	
	// ####### from here, just find typical style names and map them to the default styles ##########
	
	auto try_names_txt = [this] (WordStyleTxt style, const std::vector<const char*>& list) -> void
	{
		for (const char* i : list)
		{
			auto found = txt_style_name2id.find(i);
			if (found != txt_style_name2id.end())
			{
				txt_style_ids.insert(std::make_pair(style, found->second));
				break;
			}
		}
	};
	auto try_names_par = [this] (WordStylePar style, const std::vector<const char*>& list) -> void
	{
		for (const char* i : list)
		{
			auto found = par_style_name2id.find(i);
			if (found != par_style_name2id.end())
			{
				par_style_ids.insert(std::make_pair(style, found->second));
				break;
			}
		}
	};
	
	// try_names_txt(S_VERBATIM, {"VerbatimChar"});
	try_names_txt(S_SOURCECODE_TXT, {"Source Code", "HTML Typewriter", "HTML Code", "Verbatim"});
	try_names_txt(S_FOOTNOTE_REF, {"footnote reference"});
	try_names_txt(S_HYPERLINK, {"Hyperlink"});
	try_names_txt(S_REFERENCE, {"Reference", "Subtle Reference", "Intense Reference"}); // note: "Reference" is no predefined style, but for completeness also tried
	try_names_txt(S_PLACEHOLDER, {"Placeholder Text"});
	try_names_txt(S_EMPHASIS, {"Emphasis", "Subtle Emphasis", "Intense Emphasis"});
	try_names_txt(S_STRONG, {"Strong"});

	try_names_par(S_MAIN_TITLE, {"Title"});
	try_names_par(S_CAPTION, {"caption"});
	try_names_par(S_HEADING1, {"heading 1"});
	try_names_par(S_HEADING2, {"heading 2"});
	try_names_par(S_HEADING3, {"heading 3"});
	try_names_par(S_HEADING4, {"heading 4"});
	try_names_par(S_HEADING5, {"heading 5"});
	try_names_par(S_HEADING6, {"heading 6"});
	try_names_par(S_HEADING7, {"heading 7"});
	try_names_par(S_HEADING8, {"heading 8"});
	try_names_par(S_HEADING9, {"heading 9"});
	try_names_par(S_HEADING, {"Paragraph Heading"});
	try_names_par(S_FOOTNOTE_PAR, {"footnote text"});
	try_names_par(S_SOURCECODE_PAR, {"Source Code"});
	try_names_par(S_NO_SPACE, {"No Spacing"}); // , "Compact"
	try_names_par(S_LIST_PAR, {"List Paragraph", "List Paragraph normal"});
	try_names_par(S_LIST_PAR_COMPACT, {"List Paragraph", "List Paragraph compact"});
	try_names_par(S_ITEMIZE, {"Itemize", "Itemize normal"});
	try_names_par(S_ITEMIZE_COMPACT, {"Itemize", "Itemize compact"});
	try_names_par(S_ENUMERATE, {"Enumerate", "Enumerate normal"});
	try_names_par(S_ENUMERATE_COMPACT, {"Enumerate", "Enumerate compact"});
	// try_names_par(S_SOURCECODE_PAR, {"Source Code", "HTML Typewriter", "HTML Code"});
}

int WordFile::nextBookmarkId()
{
	int result = 0;
	static const pugi::xpath_query query_bookmark("/w:document/descendant::w:bookmarkStart");
	
	pugi::xpath_node_set set_bookmarks = query_bookmark.evaluate_node_set(xml_document);
	for (const pugi::xpath_node i : set_bookmarks)
	{
		int id = i.node().attribute("w:id").as_int();
		if (id >= result) result = id+1;
	}
	return result;
}

int WordFile::nextFootnoteId()
{
	int result = 1; // -1 and 0 are reserved TODO
	static const pugi::xpath_query query_f("/w:footnotes/w:footnote");
	
	pugi::xpath_node_set set_f = query_f.evaluate_node_set(xml_footnotes);
	for (const pugi::xpath_node i : set_f)
	{
		int id = i.node().attribute("w:id").as_int();
		if (id >= result) result = id+1;
	}
	return result;
}


std::string WordFile::getParStyleID(WordFile::WordStylePar style) const
{
	auto found = par_style_ids.find(style);
	if (found != par_style_ids.end()) return found->second;
	else return std::string();
}

std::string WordFile::getTxtStyleID(WordFile::WordStyleTxt style) const
{
	auto found = txt_style_ids.find(style);
	if (found != txt_style_ids.end()) return found->second;
	else return std::string();
}

void WordFile::setParStyleID(WordFile::WordStylePar style, const std::string& id)
{
	if (id.length()) par_style_ids[style] = id;
	else
	{
		auto found = par_style_ids.find(style);
		if (found != par_style_ids.end()) par_style_ids.erase(found);
	}
}

void WordFile::setTxtStyleID(WordFile::WordStyleTxt style, const std::string& id)
{
	if (id.length()) txt_style_ids[style] = id;
	else
	{
		auto found = txt_style_ids.find(style);
		if (found != txt_style_ids.end()) txt_style_ids.erase(found);
	}
}

int WordFile::determineListStyleNumberingId(WordFile::WordStylePar from_that_style)
{
	const int invalid_result = -1;
	
	std::string style_name = getParStyleID(from_that_style);
	const char* style_name_c = style_name.c_str();
	if (style_name.empty()) return invalid_result;
	
	pugi::xml_node styles_root = xml_styles.child("w:styles");
	if (!styles_root) return invalid_result;
	
	pugi::xml_node found_style = styles_root.find_child( [style_name_c](pugi::xml_node n) -> bool
	{
		if (strcmp(n.name(), "w:style") != 0) return false;
		if (strcmp(n.attribute("w:type").as_string(), "paragraph") != 0) return false;
		if (strcmp(n.attribute("w:styleId").as_string(), style_name_c) != 0) return false;
		return true;
	} );
	if (!found_style) return invalid_result;
	
	pugi::xml_node numId_node = found_style.child("w:pPr").child("w:numPr").child("w:numId");
	if (!numId_node) return invalid_result;
	
	pugi::xml_attribute result_attr = numId_node.attribute("w:val");
	if (!result_attr) return invalid_result;
	
	return result_attr.as_int();
}

std::string WordFile::getParStyleName(WordFile::WordStylePar style) const
{
	std::string name = getParStyleID(style);
	if (!name.length()) return name;
	
	auto found = par_style_id2name.find(name);
	if (found != par_style_id2name.end()) return found->second;
	else return "";
}

std::string WordFile::getTxtStyleName(WordFile::WordStyleTxt style) const
{
	std::string name = getTxtStyleID(style);
	if (!name.length()) return name;
	
	auto found = txt_style_id2name.find(name);
	if (found != txt_style_id2name.end()) return found->second;
	else return "";
}

bool WordFile::isValidStyleId(const std::string& id)
{
	auto found = txt_style_id2name.find(id);
	return found != txt_style_id2name.end();
}

void WordFile::setParStyleName(WordFile::WordStylePar style, const std::string& name)
{
	auto found = par_style_name2id.find(name);
	if (found != par_style_name2id.end() && found->second.length()) setParStyleID(style, found->second);
}

void WordFile::setTxtStyleName(WordFile::WordStyleTxt style, const std::string& name)
{
	auto found = txt_style_name2id.find(name);
	if (found != txt_style_name2id.end() && found->second.length()) setTxtStyleID(style, found->second);
}




void WordFile::findAllFields()
{
	static pugi::xpath_query query_next_wr("following::w:r");
	static pugi::xpath_query query_child_instr("w:instrText");
	
	static pugi::xpath_query query_fldSimple("/w:document/w:body/descendant::w:fldSimple");

	static pugi::xpath_query query_fldChar_begin("/w:document/w:body/descendant::w:fldChar[@w:fldCharType='begin']");
	// static pugi::xpath_query query_fldChar_separate("following::w:fldChar[@w:fldCharType='separate']");
	static pugi::xpath_query query_fldChar_end("following::w:fldChar[@w:fldCharType='end']");
	
	fields.clear();
	
	pugi::xpath_node_set set_fldSimple = query_fldSimple.evaluate_node_set(xml_document);
	for (const pugi::xpath_node i : set_fldSimple)
	{
		Field field;
		std::string options;
		field.node_start = i.node();
		// field.node_start = field.node_end; // the algorithms know that
		
		// *** get the MERGEFIELD strings out of the XML data ***
		const std::string s = field.node_start.attribute("w:instr").as_string();
		if (!evaluateFieldName(s, field)) continue;
		
		// *** set field attributes ***
		field.node_paragraph = XMLHelper::goUpTo(field.node_start.parent(), "w:p");
		
		// *** set field type ***
		const pugi::xml_node node_wr_child = field.node_start.child("w:r");
		if (isParagrahEmpty(field.node_paragraph, node_wr_child, node_wr_child)) field.type = F_SIMPLE_PARAGRAPH;
		else field.type = F_SIMPLE_TXT;

		// *** finish ***
		fields.insert(std::make_pair(field.name, field));
	}

	pugi::xpath_node_set set_fldChar_begin = query_fldChar_begin.evaluate_node_set(xml_document);
	for (const pugi::xpath_node i : set_fldChar_begin)
	{
		Field field;
		
		const pugi::xpath_node xp_end = query_fldChar_end.evaluate_node(i.node());
		if (!xp_end || xp_end.node().parent().parent() != i.node().parent().parent()) continue; // begin and end in different paragraphs
		
		// *** get the MERGEFIELD strings out of the XML data ***
		const pugi::xpath_node xp_next_wr = query_next_wr.evaluate_node(i.node()); // the name is below the next <w:r>
		const pugi::xpath_node xp_instr_txt = query_child_instr.evaluate_node(xp_next_wr.node()); // and here enclosed into <w:instrText>
		if (!xp_instr_txt || !evaluateFieldName(xp_instr_txt.node().text().as_string(), field)) continue;
		
		
		// *** set field attributes ***
		field.node_start = XMLHelper::goUpTo(i.node(), "w:r");
		field.node_end = XMLHelper::goUpTo(xp_end.node(), "w:r");
		field.node_paragraph = XMLHelper::goUpTo(field.node_start.parent(), "w:p");
		
		// *** set field type ***
		if (isParagrahEmpty(field.node_paragraph, field.node_start, field.node_end)) field.type = F_COMPLEX_PARAGRAPH;
		else field.type = F_COMPLEX_TXT;
		
		// *** finish ***
		fields.insert(std::make_pair(field.name, field));
	}
}

bool WordFile::evaluateFieldName(const std::string raw_txt, WordFile::Field& field)
{
	// two mergefield variants:
	// MERGEFIELD  AAA  \* MERGEFORMAT ...
	// MERGEFIELD  "BBB options options..." ...
	
	static std::regex regex_mergefield("^\\s*MERGEFIELD\\s+(?:([^\"]\\S+)|\"(\\S+)\\s+([^\"]*)\")", std::regex::optimize);
	
	std::sregex_iterator itr_match_begin = std::sregex_iterator(raw_txt.begin(), raw_txt.end(), regex_mergefield);
	std::sregex_iterator itr_match_end = std::sregex_iterator();
	if (itr_match_begin != itr_match_end)
	{
		// the whole regex match would be: itr_match_begin->str();
		auto submatch_b = itr_match_begin->cbegin();
		auto submatch_e = itr_match_begin->cend();
		
		++submatch_b; // that was the whole match
		field.name = *submatch_b;
		++submatch_b;
		if (submatch_b != submatch_e && submatch_b->length())
		{
			field.name = *submatch_b; // because in this case the first was empty
			++submatch_b;
			field.options = *submatch_b;
		}
		
		return true;
	}
	else return false;
}

bool WordFile::isParagrahEmpty(const pugi::xml_node& paragraph, const pugi::xml_node& exclude_wr_from, const pugi::xml_node& exclude_wr_to)
{
	static std::regex regex_nospace("\\S", std::regex::optimize);
	static pugi::xpath_query query_wr("self::w:p/descendant::w:r");
	static pugi::xpath_query query_wt("self::w:r/w:t");

	// look into each w:wr tag, if it contains text
	
	bool inside_excluded = false; // are we inside of the area which should be excluded?
	pugi::xpath_node_set set_wr = query_wr.evaluate_node_set(paragraph);
	for (const pugi::xpath_node i : set_wr)
	{
		if (inside_excluded)
		{
			if (i.node() == exclude_wr_to) inside_excluded = false;
			continue;
		}
		else if (i.node() == exclude_wr_from)
		{
			if (exclude_wr_to 
				&& exclude_wr_to != exclude_wr_from) inside_excluded = true;
			continue;
		}
		
		// only in the case this point is reached the <w:t> tags have to be evaluated
		pugi::xpath_node_set set_wt = query_wt.evaluate_node_set(i);
		for (const pugi::xpath_node j : set_wt)
		{
			std::string str = j.node().text().as_string();
			if (std::regex_search(str, regex_nospace)) return false;
		}
	}
	return true;
}

int WordFile::headerLevelAt(const pugi::xml_node& context) const
{
	if (!context) return 0;
	static pugi::xpath_query query_paragraph_styles("preceding::w:pStyle");
	pugi::xpath_node_set set_paragrah_styles = query_paragraph_styles.evaluate_node_set(context);
	
	std::map<WordStylePar, std::string>::const_iterator found;
	const std::string invalid = "#invalid";
	const std::string h1 = ((found = par_style_ids.find(S_HEADING1)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h2 = ((found = par_style_ids.find(S_HEADING2)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h3 = ((found = par_style_ids.find(S_HEADING3)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h4 = ((found = par_style_ids.find(S_HEADING4)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h5 = ((found = par_style_ids.find(S_HEADING5)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h6 = ((found = par_style_ids.find(S_HEADING6)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h7 = ((found = par_style_ids.find(S_HEADING7)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h8 = ((found = par_style_ids.find(S_HEADING8)) != par_style_ids.end()) ? found->second : invalid;
	const std::string h9 = ((found = par_style_ids.find(S_HEADING9)) != par_style_ids.end()) ? found->second : invalid;
	
	for (const pugi::xpath_node i : set_paragrah_styles)
	{
		if (std::string(i.node().attribute("w:val").as_string()) == h1) return 1;
		else if (std::string(i.node().attribute("w:val").as_string()) == h2) return 2;
		else if (std::string(i.node().attribute("w:val").as_string()) == h3) return 3;
		else if (std::string(i.node().attribute("w:val").as_string()) == h4) return 4;
		else if (std::string(i.node().attribute("w:val").as_string()) == h5) return 5;
		else if (std::string(i.node().attribute("w:val").as_string()) == h6) return 6;
		else if (std::string(i.node().attribute("w:val").as_string()) == h7) return 7;
		else if (std::string(i.node().attribute("w:val").as_string()) == h8) return 8;
		else if (std::string(i.node().attribute("w:val").as_string()) == h9) return 9;
	}
	return 0;
}

int WordFile::headerLevelAtField(const WordFile::Field& field) const
{
	return headerLevelAt(field.node_start);
}

int WordFile::headerLevelAtField(const std::string& fieldname) const
{
	auto r = fields.equal_range(fieldname); // get a pair with begin (first) and end (second)
	if (r.first != r.second) return headerLevelAt(r.first->second.node_start);
	else return 0;
}



std::string WordFile::insertMediaImageFile(const char* this_file_name)
{
	size_t entries = wordzip->GetEntriesCount();
	const std::string interesting_names("word/media/image");
	std::size_t interesting_names_sz = interesting_names.length();
	
	unsigned long int nextid = 1;
	for (size_t i = 0; i < entries; ++i)
	{
		ZipArchiveEntry::Ptr zip_entry = wordzip->GetEntry(static_cast<int>(i));
		std::string fullname = zip_entry->GetFullName();
		if (fullname.size() <= interesting_names_sz) continue;
		if (std::strncmp(fullname.c_str(), interesting_names.c_str(), interesting_names_sz) != 0) continue;
		unsigned long int fid_int = strtoul (fullname.c_str()+interesting_names_sz, 0, 10);
		if (fid_int >= nextid) nextid = fid_int+1;
	}
	
	std::string resulting_name = "image" + std::to_string(nextid);
	const std::string current_name = std::string(this_file_name);
	size_t extension_begin = current_name.find_last_of(".");
	resulting_name += (current_name.c_str()+extension_begin);
	
	ZipArchiveEntry::Ptr new_entry = wordzip->CreateEntry("word/media/" + resulting_name);
	std::ifstream io(this_file_name, std::ios::binary);
	new_entry->SetCompressionStream(io, DeflateMethod::Create(), ZipArchiveEntry::CompressionMode::Immediate);
	io.clear();
	
	return resulting_name;
}


