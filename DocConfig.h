// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#pragma once

#include <string>
#include "WordFile.h"

class DocConfig
{
public:
	static void syncWordTemplateConfig(const std::string& config_file, WordFile& word_file);
};
