// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#include <cassert>
#include <cstring>
#include <string>
#include <cstdlib>
#include "WordBuilder.h"
#include <pugixml.hpp>


WordBuilder::WordBuilder(WordFile& for_template, WordBuilder::Options& options) : word_template(for_template), options(options)
{
	private_opts.next_bookmark_id = word_template.nextBookmarkId();
	private_opts.next_footnote_id = word_template.nextFootnoteId();
	
	for (WordFile::WordStylePar i : {WordFile::S_ITEMIZE_COMPACT, WordFile::S_ITEMIZE, WordFile::S_LIST_PAR_COMPACT, WordFile::S_LIST_PAR, WordFile::S_NO_SPACE})
	{
		if (!word_template.getParStyleID(i).empty())
		{
			private_opts.itemize_compact_style = i;
			break;
		}
	}
	for (WordFile::WordStylePar i : {WordFile::S_ITEMIZE, WordFile::S_ITEMIZE_COMPACT, WordFile::S_LIST_PAR, WordFile::S_LIST_PAR_COMPACT})
	{
		if (!word_template.getParStyleID(i).empty())
		{
			private_opts.itemize_normal_style = i;
			break;
		}
	}
	for (WordFile::WordStylePar i : {WordFile::S_ENUMERATE_COMPACT, WordFile::S_ENUMERATE, WordFile::S_LIST_PAR_COMPACT, WordFile::S_LIST_PAR, WordFile::S_NO_SPACE})
	{
		if (!word_template.getParStyleID(i).empty())
		{
			private_opts.enumerate_compact_style = i;
			break;
		}
	}
	for (WordFile::WordStylePar i : {WordFile::S_ENUMERATE, WordFile::S_ENUMERATE_COMPACT, WordFile::S_LIST_PAR, WordFile::S_LIST_PAR_COMPACT})
	{
		if (!word_template.getParStyleID(i).empty())
		{
			private_opts.enumerate_normal_style = i;
			break;
		}
	}
	
	private_opts.itemize_compact_numId = word_template.determineListStyleNumberingId(private_opts.itemize_compact_style);
	private_opts.itemize_normal_numId = word_template.determineListStyleNumberingId(private_opts.itemize_normal_style);
	private_opts.enumerate_compact_numId = word_template.determineListStyleNumberingId(private_opts.enumerate_compact_style);
	private_opts.enumerate_normal_numId = word_template.determineListStyleNumberingId(private_opts.enumerate_normal_style);
}

void WordBuilder::setOptionsForField(const WordFile::Field& field)
{
	options.section_offset = word_template.headerLevelAtField(field);
// 	options.body_width = ;
}

pugi::xml_node WordBuilder::mkpPr(pugi::xml_node parent, const WordBuilder::ParOptions& options, bool enforce)
{
	pugi::xml_node ppr;
	
	if (enforce || options.style.length() || options.ind_first_line || options.ind_hanging || options.ind_left || options.ind_right || options.keep_next)
	{
		ppr = parent.append_child("w:pPr");
		
		if (options.style.length())
		{
			pugi::xml_node pstyle = ppr.append_child("w:pStyle");
			pstyle.append_attribute("w:val") = options.style.c_str();
		}
		
		if (options.ind_first_line || options.ind_hanging || options.ind_left || options.ind_right)
		{
			pugi::xml_node ind = ppr.append_child("w:ind");
			if (options.ind_left) ind.append_attribute("w:left") = options.ind_left;
			if (options.ind_right) ind.append_attribute("w:right") = options.ind_right;
			if (options.ind_hanging) ind.append_attribute("w:hanging") = options.ind_hanging;
			if (options.ind_first_line) ind.append_attribute("w:firstLine") = options.ind_first_line;
		}
		
		if (options.keep_next) ppr.append_child("w:keepNext");
	}
	return ppr;
}

pugi::xml_node WordBuilder::mkPar(pugi::xml_node parent, const ParOptions& options)
{
	pugi::xml_node par = parent.append_child("w:p");
	pugi::xml_node ppr = mkpPr(par, options);
	return par;
}

pugi::xml_node WordBuilder::mkrPr(pugi::xml_node parent, const RunOptions& options, bool enforce)
{
	const bool has_lang = ! options.lang.empty();
	const bool has_style =  ! options.style.empty();
	const bool has_colour =  ! options.colour.empty();
	const bool has_highlight =  ! options.highlight.empty();
	const bool has_font = !options.font.ascii.empty() || !options.font.cs.empty() || !options.font.hAnsi.empty() || !options.font.hint.empty();
	pugi::xml_node rpr;
	
	if (enforce || options.charstyle || has_lang || has_style || has_colour || has_highlight || has_font || options.spacing || options.kern || options.sz || options.szCs)
	{
		rpr = parent.append_child("w:rPr");
	
		if (options.charstyle & ITALIC) rpr.append_child("w:i");
		if (options.charstyle & BOLD) rpr.append_child("w:b");
		if (options.charstyle & UNDERLINE) rpr.append_child("w:u").append_attribute("w:val") = "single";
		if (options.charstyle & STRIKE) rpr.append_child("w:strike");
		if (options.charstyle & SUPER && !(options.charstyle & SUB)) rpr.append_child("w:vertAlign").append_attribute("w:val") = "superscript";
		if (options.charstyle & SUB && !(options.charstyle & SUPER)) rpr.append_child("w:vertAlign").append_attribute("w:val") = "subscript";
		if (options.charstyle & NO_PROOF) rpr.append_child("w:noProof");
		if (options.charstyle & HIDDEN) rpr.append_child("w:vanish");
		if (has_lang & !(options.charstyle & NO_PROOF)) rpr.append_child("w:lang").append_attribute("w:val") = options.lang.c_str();
		if (has_style) rpr.append_child("w:rStyle").append_attribute("w:val") = options.style.c_str();
		if (has_colour) rpr.append_child("w:color").append_attribute("w:val") = options.colour.c_str();
		if (has_highlight) rpr.append_child("w:highlight").append_attribute("w:val") = options.highlight.c_str();
		
		if (has_font)
		{
			pugi::xml_node font_n = rpr.append_child("w:rFonts");
			if (!options.font.ascii.empty()) font_n.append_attribute("w:ascii") = options.font.ascii.c_str();
			if (!options.font.hAnsi.empty()) font_n.append_attribute("w:hAnsi") = options.font.hAnsi.c_str();
			if (!options.font.cs.empty()) font_n.append_attribute("w:cs") = options.font.cs.c_str();
			if (!options.font.hint.empty()) font_n.append_attribute("w:hint") = options.font.hint.c_str();
		}
		
		if (options.spacing) rpr.append_child("w:spacing").append_attribute("w:val") = options.spacing;
		if (options.kern) rpr.append_child("w:kern").append_attribute("w:val") = options.kern;
		if (options.sz) rpr.append_child("w:sz").append_attribute("w:val") = options.sz;
		if (options.szCs) rpr.append_child("w:szCs").append_attribute("w:val") = options.szCs;
	}

	return rpr;
}

pugi::xml_node WordBuilder::mkRun(pugi::xml_node parent, const RunOptions& options)
{
// 	pugi::xml_node run = options.before ?
// 		parent.insert_child_before("w:r", options.before) :
// 		options.after ?
// 			parent.insert_child_after("w:r", options.after) :
// 			parent.append_child("w:r");
			
	pugi::xml_node run = parent.append_child("w:r");
	pugi::xml_node rpr = mkrPr(run, options);
	return run;
}

pugi::xml_node WordBuilder::mkRunBefore(pugi::xml_node before, const RunOptions& options)
{
	pugi::xml_node run = before.parent().insert_child_before("w:r", before);
	pugi::xml_node rpr = mkrPr(run, options);
	return run;
}

pugi::xml_node WordBuilder::mkRunAfter(pugi::xml_node after, const RunOptions& options)
{
	pugi::xml_node run = after.parent().insert_child_after("w:r", after);
	pugi::xml_node rpr = mkrPr(run, options);
	return run;
}

// Create/reuse a txt node with exactly the specified options (preserve_space), and do not reuse an existing w:t block
// when "force_create_new" is set (even it is reused only if "preserve space" is the same as requested)
pugi::xml_node WordBuilder::mkTxt(pugi::xml_node parent, const char* text, bool as_cdata, bool preserve_space, bool force_create_new)
{
	pugi::xml_node wt = parent.last_child();
	
	bool create_new = true;
	
	if (!force_create_new && std::strcmp(wt.name(), "w:t") == 0)
	{
		// the parent has already a w:t node. Check, if it can be reused
		pugi::xml_attribute xmlspace_attrib = wt.attribute("xml:space");
		bool xmlspace_value = std::strcmp(xmlspace_attrib.as_string(), "preserve") == 0;
		create_new = preserve_space != xmlspace_value;
	}
	if (force_create_new || create_new)
	{
		wt = parent.append_child("w:t");
		if (preserve_space) wt.append_attribute("xml:space") = "preserve";
	}
	if (text && strlen(text)) mkData(wt, text, as_cdata);
	return wt;
}


// Create/reuse a txt node, determine the need of "preserve_space" automatically, and create a new
// node only if it is really necessary
pugi::xml_node WordBuilder::mkTxt(pugi::xml_node parent, const char* text, bool as_cdata)
{
	pugi::xml_node wt = parent.last_child();
	const size_t len = text ? std::strlen(text) : 0;
	
	bool create_new = false;
	bool preserve_space = false;
	
	if (std::strcmp(wt.name(), "w:t") == 0)
	{
		// the parent has already a w:t node. Check, if it can be reused
		pugi::xml_attribute xmlspace_attrib = wt.attribute("xml:space");
		bool xmlspace_value = std::strcmp(xmlspace_attrib.as_string(), "preserve") == 0;
		
		if (!xmlspace_value && len)
		{
			// const bool preserve_space_begin = text[0] == ' '; // this does not matter in case of append
			// size_t len = std::strlen(text);
			// const bool preserve_space_end = len && text[len-1] == ' ';
			const bool preserve_space_end = text[len-1] == ' ';
			if (preserve_space_end)
			{
				create_new = true;
				preserve_space = true;
			}
		}
		// else just append
	}
	else if (len) // no existing node to append, it is necessary to create a new one
	{
		const bool preserve_space_begin = text[0] == ' ';
		// size_t len = std::strlen(text);
		// const bool preserve_space_end = len && text[len-1] == ' ';
		const bool preserve_space_end = text[len-1] == ' ';
		
		preserve_space = preserve_space_begin || preserve_space_end;
		create_new = true;
	}
	else create_new = true;
	
	if (create_new)
	{
		wt = parent.append_child("w:t");
		if (preserve_space) wt.append_attribute("xml:space") = "preserve";
	}
	if (len) mkData(wt, text, as_cdata);
	return wt;
}



pugi::xml_node WordBuilder::mkData(pugi::xml_node parent, const char* txtdata, bool as_cdata)
{
	assert(txtdata);
	pugi::xml_node lastnode = parent.last_child();
	
	// TODO maybe we have to convert newlines as w:br or w:cr, and tabs as w:tab, outside of / same level as w:t
	
	if ((as_cdata && lastnode.type() == pugi::node_cdata) || (!as_cdata && lastnode.type() == pugi::node_pcdata))
	{
		// the already existing text can be used, and the new text appended
		pugi::xml_text xmltext = lastnode.text();
		xmltext.set(std::string(xmltext.get()).append(txtdata).c_str());
		return lastnode;
	}
	else
	{
		// the last child is not of the type needed, therfore add an additional child
		pugi::xml_node textnode = as_cdata ? parent.append_child(pugi::node_cdata) : parent.append_child(pugi::node_pcdata);
		pugi::xml_text xmltext = textnode.text();
		xmltext.set(txtdata);
		return textnode;
	}
}

pugi::xml_node WordBuilder::mkHeading(pugi::xml_node parent, const ParOptions& options, const char* text, const char* bookmark_name)
{
	if (options.style.length())
	{
		pugi::xml_node p = mkPar(parent, options);
		if (bookmark_name && strlen(bookmark_name))
		{
			//XMLPair b = mkBookmark(p, bookmark_name);
			// pugi::xml_node r = mkRun(p, b.first);
			
			//mkBookmark(p.last_child(), pugi::xml_node(), bookmark_name);
			RunOptions ro;
			ro.lang = this->options.lang;
			pugi::xml_node r = mkRun(p, ro);
			mkBookmark(r, r, bookmark_name);
			
			if (text && strlen(text)) pugi::xml_node t = mkTxt(r, text, false);
			return r;
		}
		else
		{
			pugi::xml_node r = mkRun(p);
			if (text && strlen(text)) pugi::xml_node t = mkTxt(r, text, false);
			return r;
		}
	}
	else
	{
		pugi::xml_node p = mkPar(parent);
		if (bookmark_name && strlen(bookmark_name))
		{
			// XMLPair b = mkBookmark(p, bookmark_name);
			// pugi::xml_node r = mkRun(p, b.first, CharStyle::BOLD);
			
			RunOptions ro;
			ro.charstyle = CharStyle::BOLD;
			ro.lang = this->options.lang;
			pugi::xml_node r = mkRun(p, ro);
			mkBookmark(r, r, bookmark_name);
			if (text && strlen(text)) pugi::xml_node t = mkTxt(r, text, false);
			return r;
		}
		else
		{
			RunOptions ro;
			ro.lang = this->options.lang;
			ro.charstyle = CharStyle::BOLD;
			pugi::xml_node r = mkRun(p, ro);
			if (text && strlen(text)) pugi::xml_node t = mkTxt(r, text, false);
			return r;
		}
	}
}

pugi::xml_node WordBuilder::mkHeading(pugi::xml_node parent, int level, const char* text, const char* bookmark_name)
{
	ParOptions par_options;
	std::string& stylename = par_options.style;
	int dstlevel = level + options.section_offset;
	
	if (dstlevel > 0)
	{
		WordFile::WordStylePar par_style = WordFile::WordStylePar::S_HEADING;
		switch (dstlevel)
		{
			case 1:
				par_style = WordFile::WordStylePar::S_HEADING1;
				break;
			case 2:
				par_style = WordFile::WordStylePar::S_HEADING2;
				break;
			case 3:
				par_style = WordFile::WordStylePar::S_HEADING3;
				break;
			case 4:
				par_style = WordFile::WordStylePar::S_HEADING4;
				break;
			case 5:
				par_style = WordFile::WordStylePar::S_HEADING5;
				break;
			case 6:
				par_style = WordFile::WordStylePar::S_HEADING6;
				break;
			case 7:
				par_style = WordFile::WordStylePar::S_HEADING7;
				break;
			case 8:
				par_style = WordFile::WordStylePar::S_HEADING8;
				break;
			case 9:
				par_style = WordFile::WordStylePar::S_HEADING9;
				break;
			default:
				break;
		}
// 		int section_style = WordFile::WordStylePar::S_HEADING1 + level + options.section_offset;
// 		if (section_style < WordFile::WordStylePar::S_HEADING1) section_style = WordFile::WordStylePar::S_HEADING1;
// 		if (section_style > WordFile::WordStylePar::S_HEADING) section_style = WordFile::WordStylePar::S_HEADING;
// 		stylename = word_template.getParStyleName(section_style);

		stylename = word_template.getParStyleID(par_style);
	}
	
	return mkHeading(parent, par_options, text, bookmark_name);
}

pugi::xml_node WordBuilder::mkListEntryP(pugi::xml_node parent, int numId, int level, const WordBuilder::ParOptions& options, bool make_numId_copy)
{
	pugi::xml_node p = mkPar(parent, options);
	if (make_numId_copy) numId = copyListScheme(numId);
	if (numId < 0) return p; // invalid numId
	
	// note: if it is a itemization with a given style on first level, the <w:numPr> may be ommitted (but we don't do so)
	pugi::xml_node pPr = p.child("w:pPr");
	if (!pPr) pPr = p.append_child("w:pPr");
	pugi::xml_node numPr = pPr.append_child("w:numPr");
	
	numPr.append_child("w:ilvl").append_attribute("w:val") = level; // counted from 0-8
	numPr.append_child("w:numId").append_attribute("w:val") = numId;
	
	return p;
}



void WordBuilder::mkBookmark(pugi::xml_node from_node, pugi::xml_node to_node, const char* name)
{
	assert(name && *name);
	const int bid = private_opts.next_bookmark_id++;
	
	pugi::xml_node start;
	pugi::xml_node end;
	
	if (from_node && to_node)
	{
		start = from_node.parent().insert_child_before("w:bookmarkStart", from_node);
		end = to_node.parent().insert_child_after("w:bookmarkEnd", to_node);
	}
	else if (from_node) // append after from_node
	{
		start = from_node.parent().insert_child_after("w:bookmarkStart", from_node);
		end = from_node.parent().insert_child_after("w:bookmarkEnd", start);
	}
	else if (to_node) // append as child of to_node
	{
		start = to_node.append_child("w:bookmarkStart");
		end = to_node.append_child("w:bookmarkEnd");
	}
	
	start.append_attribute("w:id") = bid;
	start.append_attribute("w:name") = name;
	end.append_attribute("w:id") = bid;
}


std::pair<pugi::xml_node, pugi::xml_node> WordBuilder::mkField(pugi::xml_node from_code, pugi::xml_node to_code, pugi::xml_node to_content, const WordBuilder::RunOptions& ro, bool lock, bool mergeformat)
{
	if (mergeformat) // this is part of the code!
	{
		pugi::xml_node foo = mkRunAfter(to_code, ro); // empty run
	
		foo = mkRunAfter(foo, ro);
		{
			pugi::xml_node bar = foo.append_child("w:instrText");
			bar.append_attribute("xml:space") = "preserve";
			bar.append_child(pugi::node_pcdata).text().set(" \\* MERGEFORMAT ");
		}
		to_code = foo;
	}
	
	pugi::xml_node start = mkRunBefore(from_code, ro);
	pugi::xml_node fldchar = start.append_child("w:fldChar");
	fldchar.append_attribute("w:fldCharType") = "begin";
	if (lock) fldchar.append_attribute("w:fldLock") = "1";
	
	pugi::xml_node foo = mkRunAfter(to_code, ro);
	foo.append_child("w:fldChar").append_attribute("w:fldCharType") = "separate";
	
	pugi::xml_node end = mkRunAfter(to_content, ro);
	end.append_child("w:fldChar").append_attribute("w:fldCharType") = "end";
	return std::make_pair(start, end);
}

std::pair<pugi::xml_node, pugi::xml_node> WordBuilder::mkField(pugi::xml_node from_content, pugi::xml_node to_content, const std::string& code, const WordBuilder::RunOptions& ro, bool lock, bool mergeformat)
{
	assert(!code.empty());
	// examples for code:
	// " PAGEREF _Ref460853847 \h " for reference to a page number
	// " NOTEREF _Ref481049350 \h " for reference to a footnote
	// " REF _Ref460853847 \h " for reference to an anchor (mkBookmark), or figure, ...
	// https://support.office.com/de-de/article/Liste-der-Feldfunktionen-in-Word-1ad6d91a-55a7-4a8d-b535-cf7888659a51s
	
	pugi::xml_node code_node = mkRunBefore(from_content, ro);
	{
		pugi::xml_node bar = code_node.append_child("w:instrText");
		bar.append_attribute("xml:space") = "preserve";
		bar.append_child(pugi::node_pcdata).text().set(code.c_str());
	}
	
	return mkField(code_node, code_node, to_content, ro, lock, mergeformat);
}

void WordBuilder::mkBookmarkRef(pugi::xml_node from_node, pugi::xml_node to_node, const char* name, const char* typestr, RunOptions ro)
{
	const std::string code = std::string(" ") + std::string(typestr) + std::string(" \"") + std::string(name) + std::string("\" \\h ");
	mkField(from_node, to_node, code, ro, false, true);
}


std::pair<pugi::xml_node, pugi::xml_node> WordBuilder::mkFigureCaption(pugi::xml_node p, const char* caption, const RunOptions& ro, const char* bookmark_name)
{
	const char* figure_translated = "Figure ";
	if (options.lang == "de-DE") figure_translated = "Abbildung ";
	std::string strcaption = std::string(caption);
	
	pugi::xml_node from = mkRun(p, ro);
	mkTxt(from, figure_translated, false, true, true);
	
	pugi::xml_node r = mkRun(p, ro);
	pugi::xml_node foo = r.append_child("w:fldChar");
	foo.append_attribute("w:fldCharType") = "begin";
	
	r = mkRun(p, ro);
	foo = r.append_child("w:instrText");
	foo.append_attribute("xml:space") = "preserve";
	foo.append_child(pugi::node_pcdata).text().set(" SEQ Abbildung \\* ARABIC "); // TODO allow to name the SEQ
	
	r = mkRun(p, ro);
	foo = r.append_child("w:fldChar");
	foo.append_attribute("w:fldCharType") = "separate";
	
	RunOptions ro_noproof;
	ro_noproof.charstyle |= CharStyle::NO_PROOF;
	r = mkRun(p, ro_noproof);
	mkTxt(r, "###", false, false, true);
	
	pugi::xml_node r_end_part_a = mkRun(p, ro);
	foo = r_end_part_a.append_child("w:fldChar");
	foo.append_attribute("w:fldCharType") = "end";
	
	if (!strcaption.empty() && strcaption != "0")
	{
		r = mkRun(p, ro);
		std::string bar = std::string(": ") + strcaption;
		mkTxt(r, bar.c_str(), false);
	}
	
	if (bookmark_name && *bookmark_name) mkBookmark(from, r_end_part_a, bookmark_name);
	return std::make_pair(from, r);
}


/*
WordBuilder::XMLPair WordBuilder::mkBookmark(pugi::xml_node parent, const char* name)
{
	assert(name && std::strlen(name));
	const int bid = private_opts.next_bookmark_id++;
	XMLPair result = XMLPair(parent.append_child("w:bookmarkStart"), parent.append_child("w:bookmarkEnd"));
	result.first.append_attribute("w:id") = bid;
	result.first.append_attribute("w:name") = name;
	result.second.append_attribute("w:id") = bid;
	return result;
}

    <w:p>
      <w:pPr>
        <w:pStyle w:val="berschrift2" />
      </w:pPr>
      <w:bookmarkStart w:id="0" w:name="_Ref460853847" />
      <w:r>
        <w:t>Überschrift 2</w:t>
      </w:r>
      <w:bookmarkEnd w:id="0" />
    </w:p>
*/

pugi::xml_node WordBuilder::mkFootnoteP(pugi::xml_document& footnote_doc, int id, const std::string& p_style, const RunOptions& ref_nr_format)
{
	pugi::xml_node footnotes = footnote_doc.child("w:footnotes");
	
	if (!footnotes)
	{
		footnotes = footnote_doc.append_child("w:footnotes");
		appendDefaultNamespaceAttribs(footnotes);
	}
	
	pugi::xml_node fnode = footnotes.append_child("w:footnote");
	fnode.append_attribute("w:id") = id;
	
	ParOptions par_options;
	par_options.style = p_style;
	pugi::xml_node fnode_p = mkPar(fnode, par_options);
	pugi::xml_node r1 = mkRun(fnode_p, ref_nr_format);
	r1.append_child("w:footnoteRef"); // this is the footnote number in the footnote text at the bottom of the page
	return fnode_p;
}

/*
  <w:footnote w:id="1">
    <w:p>
      <w:pPr>
        <w:pStyle w:val="Funotentext" />
      </w:pPr>
      <w:r>
        <w:rPr>
          <w:rStyle w:val="Funotenzeichen" />
        </w:rPr>
        <w:footnoteRef />
      </w:r>
      <w:r>
        <w:t xml:space="preserve"> Text der Fußnote</w:t>
      </w:r>
    </w:p>
  </w:footnote>
*/

void WordBuilder::mkFootnoteNr(pugi::xml_node p, int id, const RunOptions& ro, const char* bookmark_name)
{
	pugi::xml_node fnode_txt_r = mkRun(p, ro);
	pugi::xml_node fnode_txt_ref = fnode_txt_r.append_child("w:footnoteReference");
	fnode_txt_ref.append_attribute("w:id") = id;
	
	if (bookmark_name && *bookmark_name) mkBookmark(fnode_txt_r, fnode_txt_r, bookmark_name);
}

void WordBuilder::mkFootnoteRefNr(pugi::xml_node p, const char* bookmark_name, const char* txt_content, const RunOptions& ro)
{
	pugi::xml_node fnode_txt_r = mkRun(p, ro);
	mkTxt(fnode_txt_r, txt_content, false);
	mkBookmarkRef(fnode_txt_r, fnode_txt_r, bookmark_name, "NOTEREF", ro);
}

std::string WordBuilder::mkMediaRelationship(pugi::xml_document& rel_doc, const char* type, const char* target, const char* target_mode)
{
	pugi::xml_node rels = rel_doc.child("Relationships");
	
	if (!rels)
	{
		rels = rel_doc.append_child("Relationships");
		rels.append_attribute("xmlns") = "http://schemas.openxmlformats.org/package/2006/relationships";
	}
	
	unsigned long int nextid = 1;
	for (pugi::xml_node& i : rels.children("Relationship"))
	{
		const char* rid = i.attribute("Id").as_string();
		if (!rid) continue;
		// the format is "rId" followed by a number. Now skip everything which is not a number
		while (*rid && (*rid < 0x30 || *rid > 0x39)) ++rid;
		unsigned long int rid_int = strtoul (rid, 0, 10);
		if (rid_int >= nextid) nextid = rid_int+1;
	}
	
	// note: this is not the only file in this directory, but the only one where the IDs are checked.
	// therfore use an other namespace: not "rId", but "rIdx" instead
	
	std::string id = "rIdx" + std::to_string(nextid); // note the x here, to get a new namespace
	pugi::xml_node rel = rels.append_child("Relationship");
	rel.append_attribute("Id") = id.c_str();
	rel.append_attribute("Type") = type;
	rel.append_attribute("Target") = target;
	if (target_mode && *target_mode) rel.append_attribute("TargetMode") = target_mode;
	return id;
}

std::string WordBuilder::mkImageRelationship(pugi::xml_document& rel_doc, const char* target)
{
	return mkMediaRelationship(rel_doc, "http://schemas.openxmlformats.org/officeDocument/2006/relationships/image", target);
}



pugi::xml_node WordBuilder::mkInlineDrawing(pugi::xml_node r, unsigned int widht_emu, unsigned int height_emu, int id)
{
	pugi::xml_node drawing = r.append_child("w:drawing");
	pugi::xml_node drawing_inline = drawing.append_child("wp:inline");
	pugi::xml_node extent = drawing_inline.append_child("wp:extent");
	// English Metric Units (EMUs): 1/360,000 of a centimeter and thus there are 914,400 EMUs per inch, and 12,700 EMUs per point
	extent.append_attribute("cx") = widht_emu;
	extent.append_attribute("cy") = height_emu;
// 	pugi::xml_node eextent = drawing_inline.append_child("wp:effectExtent");
// 	eextent.append_attribute("l") = "0"; // left
// 	eextent.append_attribute("t") = "0"; // top
// 	eextent.append_attribute("r") = "2540"; // right
// 	eextent.append_attribute("b") = "1000"; // bottom
	
	
	pugi::xml_node docPr = drawing_inline.append_child("wp:docPr");
	// this id MUST be distinct in the word document
	docPr.append_attribute("id") = id; // mandatory
	docPr.append_attribute("name") = "Picture"; // mandatory
	// docPr.append_attribute("title") = ###;
	
	pugi::xml_node locking_props_area = drawing_inline.append_child("wp:cNvGraphicFramePr");
	pugi::xml_node locking_props = locking_props_area.append_child("a:graphicFrameLocks");
	locking_props.append_attribute("xmlns:a") = "http://schemas.openxmlformats.org/drawingml/2006/main";
	locking_props.append_attribute("noChangeAspect") = "1";
	
	return drawing_inline;
}




pugi::xml_node WordBuilder::mkGraphic(pugi::xml_node wp_container, unsigned int widht_emu, unsigned int height_emu, const char* blipId, bool embed, int id)
{
	pugi::xml_node graphic = wp_container.append_child("a:graphic");
	graphic.append_attribute("xmlns:a") = "http://schemas.openxmlformats.org/drawingml/2006/main";
	pugi::xml_node gdata = graphic.append_child("a:graphicData");
	gdata.append_attribute("uri") = "http://schemas.openxmlformats.org/drawingml/2006/picture";
	pugi::xml_node pic = gdata.append_child("pic:pic");
	pic.append_attribute("xmlns:pic") = "http://schemas.openxmlformats.org/drawingml/2006/picture";
	
	pugi::xml_node picprops = pic.append_child("pic:nvPicPr");
	pugi::xml_node cNvPr = picprops.append_child("pic:cNvPr");
	// here defaults can be used, which are always valid, because in this canvas is only one picture
	cNvPr.append_attribute("id") = id; // the only one in the canvas
	cNvPr.append_attribute("name") = ""; // "Picture #" would also be typical
	// cNvPr.append_attribute("title") = ###; // does this attribute really exist? Some documentations are missing it
	// cNvPr.append_attribute("descr") = ###;
	pugi::xml_node pic_locking_props_area = picprops.append_child("pic:cNvPicPr");
	pugi::xml_node pic_locking_props = pic_locking_props_area.append_child("a:picLocks");
	pic_locking_props.append_attribute("noChangeAspect") = "1";
	pic_locking_props.append_attribute("noChangeArrowheads") = "1";
	
	pugi::xml_node blipFill = pic.append_child("pic:blipFill");
	pugi::xml_node blip = blipFill.append_child("a:blip"); // BLIP = binary large image or picture
	
	if (embed) blip.append_attribute("r:embed") = blipId; // <Relationship Id="rID4" ...>
	else blip.append_attribute("r:link") = blipId; // <Relationship Id="rID4" ...>
	// blip.append_attribute("r:cstate") = "screen"; // compression for: email, hqprint, print, screen
	
	blipFill.append_child("a:srcRect"); // no children or attributes, because no cropping is applied
	pugi::xml_node stretch = blipFill.append_child("a:stretch");
	pugi::xml_node stretch_fill = stretch.append_child("a:fillRect");
	
	pugi::xml_node shape_props = pic.append_child("pic:spPr");
	shape_props.append_attribute("bwMode") = "auto"; // black/white mode
	
	pugi::xml_node xfrm = shape_props.append_child("a:xfrm");
	pugi::xml_node off = xfrm.append_child("a:off");
	off.append_attribute("x") = 0;
	off.append_attribute("y") = 0;
	pugi::xml_node ext = xfrm.append_child("a:ext");
	ext.append_attribute("cx") = widht_emu;
	ext.append_attribute("cy") = height_emu;
	pugi::xml_node prstGeom = shape_props.append_child("a:prstGeom");
	prstGeom.append_attribute("prst") = "rect";
	prstGeom.append_child("a:avLst");
	shape_props.append_child("a:noFill");
	pugi::xml_node line_props = shape_props.append_child("a:ln");
	line_props.append_child("a:noFill");
	
	return graphic;
}


std::pair<int, pugi::xml_node> WordBuilder::declareNewListScheme()
{
	pugi::xml_document& doc = word_template.getNumberingDoc();
	int abstractNumId = findMaxId(doc, "/w:numbering/w:abstractNum", "w:abstractNumId") + 1;
	int numId = findMaxId(doc, "/w:numbering/w:num", "w:numId") + 1;
	
	pugi::xml_node numbering = doc.child("w:numbering");
	if (!numbering)
	{
		numbering = doc.append_child("w:numbering");
		appendDefaultNamespaceAttribs(numbering);
	}
	
	// note: The order of the xml nodes seems to be important for word. First the "w:abstractNum" nodes, and then the "w:num"!!
	pugi::xml_node abstractNum;
	pugi::xml_node first_existing_numId_node = numbering.child("w:num");
	if (first_existing_numId_node) abstractNum = numbering.insert_child_before("w:abstractNum", first_existing_numId_node);
	else abstractNum = numbering.append_child("w:abstractNum");
	abstractNum.append_attribute("w:abstractNumId") = abstractNumId;
	
	pugi::xml_node num = numbering.append_child("w:num");
	num.append_attribute("w:numId") = numId;
	num.append_child("w:abstractNumId").append_attribute("w:val") = abstractNumId;
	
	return std::make_pair(numId, abstractNum);
}

void WordBuilder::implementListScheme(pugi::xml_node abstractNum_node, const WordBuilder::ListScheme& scheme)
{
	// https://msdn.microsoft.com/en-us/library/documentformat.openxml.wordprocessing.nsid.aspx
	// the following hex number is fixed, but should be distinct for this use case
	if (!scheme.nsid.empty()) abstractNum_node.append_child("w:nsid").append_attribute("w:val") = scheme.nsid.c_str();
	
	// https://msdn.microsoft.com/en-us/library/documentformat.openxml.wordprocessing.templatecode(v=office.14).aspx
	// abstractNum_node.append_child("w:tmpl").append_attribute("w:val") = "7ef2c849"; // unknown if this is needed. only for numbering?
	
	if (!scheme.multiLevelType.empty()) abstractNum_node.append_child("w:multiLevelType").append_attribute("w:val") = scheme.multiLevelType.c_str();
	
	for (int i = 0; i < scheme.size; ++i)
	{
		const ListLevel& current = scheme.levels[i];
		
		pugi::xml_node lvl = abstractNum_node.append_child("w:lvl");
		lvl.append_attribute("w:ilvl") = i;
		lvl.append_child("w:start").append_attribute("w:val") = current.start;
		
		lvl.append_child("w:numFmt").append_attribute("w:val") = current.numFmt.c_str();
		if (!current.pStyle.empty()) lvl.append_child("w:pStyle").append_attribute("w:val") = current.pStyle.c_str();
		lvl.append_child("w:lvlText").append_attribute("w:val") = current.lvlText.c_str();
		lvl.append_child("w:lvlJc").append_attribute("w:val") = current.lvlJc.c_str();
		
		pugi::xml_node pPr = mkpPr(lvl, current.par_options);
		pugi::xml_node rPr = mkrPr(lvl, current.run_options);
	}
/*
// pandoc itemize style:
// 	<w:abstractNum w:abstractNumId="0">
//     <w:multiLevelType w:val="multilevel" />
//     <w:lvl w:ilvl="0">
//       <w:numFmt w:val="bullet" />
//       <w:lvlText w:val=" " />
//       <w:lvlJc w:val="left" />
//       <w:pPr>
//         <w:tabs>
//           <w:tab w:val="num" w:pos="0" />
//         </w:tabs>
//         <w:ind w:left="480" w:hanging="480" />
//       </w:pPr>
//     </w:lvl>
	
	for (int i = 0; i < 1; ++i)
	{
		pugi::xml_node lvl = abstractNum_node.append_child("w:lvl").append_attribute("w:ilvl") = i;
		lvl.append_child("w:numFmt").append_attribute("w:val") = "bullet";
		lvl.append_child("w:lvlText").append_attribute("w:val") = " "; // TODO
		lvl.append_child("w:lvlJc").append_attribute("w:val") = "left";
		
		pugi::xml_node pPr = abstractNum_node.append_child("w:pPr");
		pugi::xml_node tabs = pPr.append_child("w:tabs");
		pugi::xml_node tab = tab.append_child("w:tab").append_attribute("w:val") = "num";
		tab.append_attribute("w:pos") = 0;
		pugi::xml_node ind = pPr.append_child("w:ind");
		ind.append_attribute("w:left") = 480;
		ind.append_attribute("w:hanging") = 480;
	}
*/
}

void WordBuilder::initDefaultEnumerateLevel(WordBuilder::ListLevel& that, int level, int indent_level)
{
	if (indent_level < 0) indent_level = level;
	
	that.is_enumeration = true;
	that.start = 1;
	that.lvlJc = "left";
	that.pStyle = "";
	that.par_options.ind_left = 360*(indent_level+1);
	that.par_options.ind_hanging = 360;
// 	that.run_options.font.ascii = "Symbol";
// 	that.run_options.font.hAnsi = "Symbol";
	that.run_options.font.hint = "default";
	
	// Format: decimal/upperLetter/lowerLetter/upperRoman/lowerRoman or just "bullet"
	// Text: for example on level 3: "%1.%2.%3" to include the lower levels too
	
	const std::string indent_level_str = std::to_string(indent_level+1);
	
	switch (level)
	{
	case 0:
		that.numFmt = "decimal";
		that.lvlText = "%" + indent_level_str + ".";
		break;
	case 1:
		that.numFmt = "upperLetter";
		that.lvlText = "%" + indent_level_str + ".";
		break;
	case 2:
		that.numFmt = "lowerLetter";
		that.lvlText = "%" + indent_level_str + ")";
		break;
	case 3:
		that.numFmt = "upperRoman";
		that.lvlText = "%" + indent_level_str + ".";
		break;
	case 4:
		that.numFmt = "lowerRoman";
		that.lvlText = "%" + indent_level_str + ")";
		break;
	case 5:
		that.numFmt = "decimal";
		that.lvlText = "%" + indent_level_str + "*";
		break;
	case 6:
		that.numFmt = "upperLetter";
		that.lvlText = "%" + indent_level_str + "*";
		break;
	case 7:
		that.numFmt = "lowerLetter";
		that.lvlText = "%" + indent_level_str + "*";
		break;
	case 8:
		that.numFmt = "upperRoman";
		that.lvlText = "%" + indent_level_str + "*";
		break;
	default:
		assert(false); // this case must not happen, because there are only 9 levels
		break;
	}
}

void WordBuilder::initDefaultItemizeLevel(WordBuilder::ListLevel& that, int level, int indent_level)
{
	if (indent_level < 0) indent_level = level;
	
	that.is_enumeration = false;
	that.start = 1;
	that.numFmt = "bullet";
	that.lvlJc = "left";
	that.pStyle = "";
	that.par_options.ind_left = 360*(indent_level+1);
	that.par_options.ind_hanging = 360;
	that.run_options.font.hint = "default";
	
	switch (level)
	{
	case 0:
	case 4:
	case 8:
		that.lvlText = "\xEF\x82\xB7"; // filled bullet
		that.run_options.font.ascii = "Symbol";
		that.run_options.font.hAnsi = "Symbol";
		break;
	case 1:
	case 5:
		that.lvlText = "o"; // unfilled bullet
		that.run_options.font.ascii = "Courier New";
		that.run_options.font.hAnsi = "Courier New";
		break;
	case 2:
	case 6:
		that.lvlText = "\xEF\x82\xA7"; // filled square
		that.run_options.font.ascii = "Wingdings";
		that.run_options.font.hAnsi = "Wingdings";
		break;
	case 3:
	case 7:
		that.lvlText = "\xEF\x80\xAD"; // hyphen 
		that.run_options.font.ascii = "Symbol";
		that.run_options.font.hAnsi = "Symbol";
		break;
	default:
		assert(false); // this case must not happen, because there are only 9 levels
		break;
	}
}

WordBuilder::ListScheme WordBuilder::createDefaultEnumerateScheme()
{
	WordBuilder::ListScheme scheme;
	const int size = 9; // counted from 0 to 9
	
	scheme.nsid = "7ef2c847"; // random number for this specific default scheme - change/remove it, if anything is changed afterwards
	scheme.multiLevelType = "hybridMultilevel"; // "multiLevel" would have the same symbol for each level
	scheme.size = size;
	// scheme.is_default = true;
	
	
	for (int i=0; i<size; i++) initDefaultEnumerateLevel(scheme.levels[i], i);
	return scheme;
}

WordBuilder::ListScheme WordBuilder::createDefaultItemizeScheme()
{
	WordBuilder::ListScheme scheme;
	const int size = 9; // counted from 0 to 9
	
	scheme.nsid = "7ef2c849"; // random number for this specific default scheme - change/remove it, if anything is changed afterwards
	scheme.multiLevelType = "hybridMultilevel"; // "multiLevel" would have the same numbering scheme for each level
	scheme.size = size;
	// scheme.is_default = true;
	
	for (int i=0; i<size; i++) initDefaultItemizeLevel(scheme.levels[i], i);
	return scheme;
}


int WordBuilder::copyListScheme(int numId, const int start_counter_array[])
{
	pugi::xml_document& doc = word_template.getNumberingDoc();
	
	pugi::xml_node numbering = doc.child("w:numbering");
	if (!numbering) return -1;
	
	pugi::xml_node found_srcNumId = numbering.find_child( [numId](pugi::xml_node n) -> bool { return strcmp(n.name(), "w:num") == 0 && n.attribute("w:numId").as_int() == numId; } );
	if (!found_srcNumId) return -1;
	
	pugi::xml_node found_srcAbstractNumId = found_srcNumId.child("w:abstractNumId");
	if (!found_srcNumId) return -1;
	
	int src_abstractNumId = found_srcAbstractNumId.attribute("w:val").as_int();
	pugi::xml_node found_abstractNum = numbering.find_child(
		[src_abstractNumId](pugi::xml_node n) -> bool { return strcmp(n.name(), "w:abstractNum") == 0 && n.attribute("w:abstractNumId").as_int() == src_abstractNumId; } );
	if (!found_abstractNum) return -1;
	
	int abstractNumId = findMaxId(doc, "/w:numbering/w:abstractNum", "w:abstractNumId") + 1;
	int new_numId = findMaxId(doc, "/w:numbering/w:num", "w:numId") + 1;
	
	pugi::xml_node first_existing_numId_node = numbering.child("w:num");
	if (first_existing_numId_node)
	{
		pugi::xml_node new_abstractNum = numbering.insert_copy_before(found_abstractNum, first_existing_numId_node);
		new_abstractNum.attribute("w:abstractNumId") = abstractNumId;
	}
	else
	{
		pugi::xml_node new_abstractNum = numbering.append_copy(found_abstractNum);
		new_abstractNum.attribute("w:abstractNumId") = abstractNumId;
	}
	
	pugi::xml_node new_num = numbering.append_child("w:num");
	new_num.append_attribute("w:numId") = new_numId;
	new_num.append_child("w:abstractNumId").append_attribute("w:val") = abstractNumId;
	
	
	const int default_start_array[] = { 1, 1, 1, 1, 1, 1, 1, 1, 1 };
	const int *start_array = start_counter_array ? start_counter_array : default_start_array;
	
	int level = 0;
	while (level <= 9 && start_array[level] >= 0)
	{
		pugi::xml_node found_lvl= numbering.find_child( [level](pugi::xml_node n) -> bool { return strcmp(n.name(), "w:lvl") == 0 && n.attribute("w:ilvl").as_int() == level; } );
		if (!found_lvl) break;
		
		pugi::xml_node n_start = found_lvl.child("w:start");
		if (!n_start) n_start = found_lvl.append_child("w:start");
		
		pugi::xml_attribute startval = n_start.attribute("w:val");
		if (!startval) startval = n_start.append_attribute("w:val");
		
		startval = start_array[level];
		
		++level;
	}

	
	return new_numId;
}



int WordBuilder::findMaxId(const pugi::xpath_node& node, const char* query, const char* attribute, int start_id)
{
	int &result = start_id;
	const pugi::xpath_query query_object(query);
	const pugi::xpath_node_set set = query_object.evaluate_node_set(node);
	for (const pugi::xpath_node i : set)
	{
		int id = i.node().attribute(attribute).as_int();
		if (id >= result) result = id;
	}
	return result;
}

void WordBuilder::ltrim(std::string& str)
{	
	auto b = str.cbegin();
	const auto ex = str.cend();
	size_t new_begin = 0;
	for (; b != ex; ++b)
	{
		if (*b != ' ' && *b != '\t' && *b != '\n' && *b != '\r' && *b != '\v' && *b != '\f') break;
		++new_begin;
	}
	str.erase(0, new_begin);
}

void WordBuilder::rtrim(std::string& str)
{
	auto rb = str.crbegin();
	const auto rex = str.crend();
	size_t new_len = str.length();
	for (; rb != rex; ++rb)
	{
		if (*rb != ' ' && *rb != '\t' && *rb != '\n' && *rb != '\r' && *rb != '\v' && *rb != '\f') break;
		--new_len;
	}
	str.resize(new_len);
}

void WordBuilder::trim(std::string& str)
{
	ltrim(str);
	rtrim(str);
}


void WordBuilder::appendDefaultNamespaceAttribs(pugi::xml_node node)
{
	node.append_attribute("xmlns:wpc") = "http://schemas.microsoft.com/office/word/2010/wordprocessingCanvas";
	node.append_attribute("xmlns:mc") = "http://schemas.openxmlformats.org/markup-compatibility/2006";
	node.append_attribute("xmlns:o") = "urn:schemas-microsoft-com:office:office";
	node.append_attribute("xmlns:r") = "http://schemas.openxmlformats.org/officeDocument/2006/relationships";
	node.append_attribute("xmlns:m") = "http://schemas.openxmlformats.org/officeDocument/2006/math";
	node.append_attribute("xmlns:v") = "urn:schemas-microsoft-com:vml";
	node.append_attribute("xmlns:wp14") = "http://schemas.microsoft.com/office/word/2010/wordprocessingDrawing";
	node.append_attribute("xmlns:wp") = "http://schemas.openxmlformats.org/drawingml/2006/wordprocessingDrawing";
	node.append_attribute("xmlns:w10") = "urn:schemas-microsoft-com:office:word";
	node.append_attribute("xmlns:w") = "http://schemas.openxmlformats.org/wordprocessingml/2006/main";
	node.append_attribute("xmlns:w14") = "http://schemas.microsoft.com/office/word/2010/wordml";
	node.append_attribute("xmlns:wpg") = "http://schemas.microsoft.com/office/word/2010/wordprocessingGroup";
	node.append_attribute("xmlns:wpi") = "http://schemas.microsoft.com/office/word/2010/wordprocessingInk";
	node.append_attribute("xmlns:wne") = "http://schemas.microsoft.com/office/word/2006/wordml";
	node.append_attribute("xmlns:wps") = "http://schemas.microsoft.com/office/word/2010/wordprocessingShape";
	node.append_attribute("mc:Ignorable") = "w14 wp14";
}

void WordBuilder::ensureContentTypesExtension(pugi::xml_document& content_types_doc, const char* extension, const char* content_type)
{
	pugi::xml_node root = content_types_doc.child("Types");
	
	if (!root)
	{
		root = content_types_doc.append_child("Types");
		root.append_attribute("xmlns");
	}

	for (pugi::xml_node i : root.children("Default"))
	{
		if (strcmp(i.attribute("Extension").as_string(), extension) == 0) return;
	}
	
	pugi::xml_node entry = root.append_child("Default");
	entry.append_attribute("Extension") = extension;
	entry.append_attribute("ContentType") = content_type;
}
