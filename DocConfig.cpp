// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */


#include <SimpleIni.h>
#include "DocConfig.h"

void DocConfig::syncWordTemplateConfig(const std::string& config_file, WordFile& word_file)
{
	CSimpleIniA ini(true, false, false);
	ini.LoadFile(config_file.c_str());
	
	
	const char* conf_txt = "Character Styles";
	const char* auto_txt = "determined Character Styles";

	auto setP = [&ini, &word_file] (WordFile::WordStylePar cppkey, const std::string& strkey) -> void
	{
		std::string auto_val;
		std::string conf_val;
		const char* conf_col = "Paragraph Styles";
// 		const char* auto_col = "determined Paragraph Styles";
		
		auto_val = word_file.getParStyleName(cppkey);
// 		ini.SetValue(auto_col, strkey.c_str(), auto_val.c_str());
		conf_val = ini.GetValue(conf_col, strkey.c_str(), " ");
		if (conf_val == " ") ini.SetValue(conf_col, strkey.c_str(), auto_val.c_str());
		else word_file.setParStyleName(cppkey, conf_val);
	};
	
	auto setT = [&ini, &word_file] (WordFile::WordStyleTxt cppkey, const std::string& strkey) -> void
	{
		std::string auto_val;
		std::string conf_val;
		const char* conf_col = "Text Styles";
// 		const char* auto_col = "determined Text Styles";
		
		auto_val = word_file.getTxtStyleName(cppkey);
// 		ini.SetValue(auto_col, strkey.c_str(), auto_val.c_str());
		conf_val = ini.GetValue(conf_col, strkey.c_str(), " ");
		if (conf_val == " ") ini.SetValue(conf_col, strkey.c_str(), auto_val.c_str());
		else word_file.setTxtStyleName(cppkey, conf_val);
	};
	
	setP(WordFile::WordStylePar::S_MAIN_TITLE, "Main Title");
	setP(WordFile::WordStylePar::S_CAPTION, "Caption");
	setP(WordFile::WordStylePar::S_HEADING1, "Heading 1");
	setP(WordFile::WordStylePar::S_HEADING2, "Heading 2");
	setP(WordFile::WordStylePar::S_HEADING3, "Heading 3");
	setP(WordFile::WordStylePar::S_HEADING4, "Heading 4");
	setP(WordFile::WordStylePar::S_HEADING5, "Heading 5");
	setP(WordFile::WordStylePar::S_HEADING6, "Heading 6");
	setP(WordFile::WordStylePar::S_HEADING7, "Heading 7");
	setP(WordFile::WordStylePar::S_HEADING8, "Heading 8");
	setP(WordFile::WordStylePar::S_HEADING9, "Heading 9");
	setP(WordFile::WordStylePar::S_HEADING, "Heading");
	setP(WordFile::WordStylePar::S_FOOTNOTE_PAR, "Footnote Text");
	setP(WordFile::WordStylePar::S_SOURCECODE_PAR, "Source Code");
	setP(WordFile::WordStylePar::S_NO_SPACE, "No Spacing");
	setP(WordFile::WordStylePar::S_LIST_PAR, "List Paragraph");
	setP(WordFile::WordStylePar::S_LIST_PAR_COMPACT, "List Paragraph Compact");
	setP(WordFile::WordStylePar::S_ITEMIZE, "Itemize");
	setP(WordFile::WordStylePar::S_ITEMIZE_COMPACT, "Itemize Compact");
	setP(WordFile::WordStylePar::S_ENUMERATE, "Enumerate");
	setP(WordFile::WordStylePar::S_ENUMERATE_COMPACT, "Enumerate Compact");
	
	setT(WordFile::WordStyleTxt::S_SOURCECODE_TXT, "Source Code");
	setT(WordFile::WordStyleTxt::S_FOOTNOTE_REF, "Footnote Number");
	setT(WordFile::WordStyleTxt::S_HYPERLINK, "Hyperlink");
	setT(WordFile::WordStyleTxt::S_REFERENCE, "Reference");
	setT(WordFile::WordStyleTxt::S_PLACEHOLDER, "Placeholder");
	setT(WordFile::WordStyleTxt::S_EMPHASIS, "Emphasis");
	setT(WordFile::WordStyleTxt::S_STRONG, "Strong");
	
	/*
		- source_code_line_breaks (auf 0 wenn S_NO_SPACE oder S_SOURCECODE_PAR existiert, sonst 1)
		- empty line after sourcecode (auf 1 wenn S_SOURCECODE_PAR existiert, sonst 0, weil dann die letzte Zeile immer einfach ohne S_NO_SPACE gemacht wird)
		
	 */
	
	ini.SaveFile(config_file.c_str(), false);
}

