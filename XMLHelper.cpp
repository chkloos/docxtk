// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
* Copyright (c) Christoph Kloos 2018
* This file is subject to the terms and conditions of the
* MIT License | https://opensource.org/licenses/MIT
* SPDX-License-Identifier: MIT
*/

#include <cstring>
#include "XMLHelper.h"

namespace XMLHelper
{

	pugi::xml_node goUpTo(pugi::xml_node start_here, const char* to_name)
	{
		// const pugi::xml_node root = start_here.root();
		pugi::xml_node current = start_here;
		
		do
		{
			const char* tag_name = current.name();
			if (std::strcmp(tag_name, to_name) == 0) return current;
			current = current.parent();
		} while (current);
		return current; // in this case a NULL node
	}
	
}
