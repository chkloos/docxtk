// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
 * Copyright (c) Christoph Kloos 2018
 * This file is subject to the terms and conditions of the
 * GNU GPL Version 3.0 | http://www.gnu.org/licenses/
 * SPDX-License-Identifier: GPL-3.0
 */

#pragma once

#include <map>
#include <string>
#include <ZipArchive.h>
#include <ZipArchiveEntry.h>
#include <pugixml.hpp>


class WordFile
{
public:
	enum WordStylePar
	{
		S_INVALID_PAR_STYLE,
		S_MAIN_TITLE, // "Title" | Titel
		S_CAPTION, // "caption" for pictures etc. | Beschriftung
		S_HEADING1,
		S_HEADING2,
		S_HEADING3,
		S_HEADING4,
		S_HEADING5,
		S_HEADING6,
		S_HEADING7,
		S_HEADING8,
		S_HEADING9,
		S_HEADING, // (custom: "Paragraph Heading")
		S_FOOTNOTE_PAR, // "footnote text"
		S_SOURCECODE_PAR, // (custom: "Source Code")
		S_NO_SPACE, // "No Spacing", "Kein Leeraum" ; name="No Spacing" / id="KeinLeerraum" ("Compact" / "Compact") | Kein Leeraum
		S_LIST_PAR, // "List Paragraph" | Listenabsatz
		S_LIST_PAR_COMPACT,
		S_ITEMIZE, // (custom: "Itemize")
		S_ITEMIZE_COMPACT,
		S_ENUMERATE, // (custom: "Enumerate")
		S_ENUMERATE_COMPACT
	};
	
	enum WordStyleTxt
	{
		S_INVALID_TXT_STYLE,
		S_SOURCECODE_TXT, // (custom: "Source Code"), "HTML Typewriter", HTML Code" ;; | "HTML Schreibmaschine", "HTML Code"
		S_FOOTNOTE_REF, // "footnote reference"
		S_HYPERLINK, // "Hyperlink"
		S_REFERENCE, // "Subtle Reference", "Intense Reference" | Schwacher Verweis, Intensiver Verweis
		S_PLACEHOLDER, // "Placeholder Text" | Platzhaltertext
		S_EMPHASIS, // "Emphasis", "Subtle Emphasis", "Intense Emphasis", or just italic as replacement | Hervorhebung, Schwache ~, Intensive ~
		S_STRONG, // "Strong", or just bold as replacement | Fett
		// S_VERBATIM,
	};
	
	int nextBookmarkId();
	int nextFootnoteId();
	
	
private:
	ZipArchive::Ptr wordzip;
	
	pugi::xml_document xml_document; // "word/document.xml"
	pugi::xml_document xml_styles; // "word/styles.xml"
	pugi::xml_document xml_numbering; // "word/numbering.xml"
	pugi::xml_document xml_footnotes; // "word/footnotes.xml"
	pugi::xml_document xml_rels; // "word/_rels/document.xml.rels"
	pugi::xml_document xml_content_types; // "[Content_Types].xml"
	
	
	
	bool load_xml_doc(pugi::xml_document& xml_doc, const std::string& entry_name);
	bool save_xml_doc(pugi::xml_document& xml_doc, const std::string& entry, bool pretty = false);
	
	std::map<std::string, std::string> par_style_name2id;
	std::map<std::string, std::string> txt_style_name2id;
	std::map<std::string, std::string> par_style_id2name;
	std::map<std::string, std::string> txt_style_id2name;
	std::map<WordStylePar, std::string> par_style_ids;
	std::map<WordStyleTxt, std::string> txt_style_ids;
	void populateStyles();

	
	enum FieldType
	{
		F_UNKNOWN,
		
		// SIMPLE case
		// node_start is <w:fldSimple>, the <w:r> is inside of <w:fldSimple>
		// context: "<w:p><w:r>...</w:r><w:fldSimple><w:r><w:rPr>Styles</w:rPr></w:r>...</w:fldSimple><something/></w:p>
		F_SIMPLE_PARAGRAPH, // the whole paragrah has to be replaced
		
		// the whole <w:fldSimple> have to be replaced by a new construction <w:r>...</w:r>,
		// but the styles in the first <w:rPr> have to be kept
		F_SIMPLE_TXT,
		
		// COMPLEX case
		// node_start/node_end are the <w:r> tags, where the <w:fldChar w:fldCharType="begin or end" /> are inside!
		// context: <w:p>....
		//		<w:r><w:rPr>STYLE</w:rPr><w:fldChar w:fldCharType="begin" /></w:r>
		//		<w:r><w:instrText xml:space="preserve"> MERGEFIELD  abcde  \* MERGEFORMAT </w:instrText></w:r>
		// 		<w:r><w:fldChar w:fldCharType="separate" /></w:r>
		//		<w:r>CURRENT TEXT</w:r>
		//		<w:r><w:fldChar w:fldCharType="end" /></w:r>
		// ... <w:p>
		// it has to be ensured, that begin and end does not span over more than one paragraph.
		
		F_COMPLEX_PARAGRAPH, // In case of F_COMPLEX_PARAGRAPH, the whole parent paragraph has to be replaced.
		
		
		// In case of F_COMPLEX_TXT, every <w:r> containing to this area have to be replaced. the style of <w:r>
		// which contains the begin (<w:fldChar...>) has to be overtaken (but only the style "<w:rPr>"
		F_COMPLEX_TXT,
		
		
// 		F_TBL_V, // table: vertical
// 		F_TBL_VL, // table: vertical limited (limited to already existing rows, do not append new rows)
	};
	
public:
	struct Field
	{
		std::string name;
		pugi::xml_node node_start;
		pugi::xml_node node_end;
		pugi::xml_node node_paragraph;
		FieldType type = F_UNKNOWN;
		std::string options;
		
		// TODO std::vector<FieldType> repeat_content; // type=F_REPEAT
	};
	
private:
	
	std::multimap<std::string, Field> fields;
	void findAllFields();
	static bool evaluateFieldName(const std::string raw_txt, Field &field);
	static bool isParagrahEmpty(const pugi::xml_node& paragraph,
		const pugi::xml_node& exclude_wr_from, const pugi::xml_node& exclude_wr_to = pugi::xml_node());
	
	int headerLevelAt(const pugi::xml_node& here) const;
	
public:
	
// 	typedef void* FieldPT; // field private type, which prohibits external access
	
	bool open(const std::string& path);
	bool saveAs(const std::string& path, bool pretty = false);
	
	pugi::xml_node getDocumentBody();
	pugi::xml_document& getFootnoteDoc();
	pugi::xml_document& getNumberingDoc();
	pugi::xml_document& getRelsDoc();
	pugi::xml_document& getContentTypesDoc();
	
	std::string getParStyleID(WordStylePar style) const;
	std::string getTxtStyleID(WordStyleTxt style) const;
	void setParStyleID(WordStylePar style, const std::string& id);
	void setTxtStyleID(WordStyleTxt style, const std::string& id);
	std::string getParStyleName(WordStylePar style) const;
	std::string getTxtStyleName(WordStyleTxt style) const;
	bool isValidStyleId(const std::string& id);
	void setParStyleName(WordStylePar style, const std::string& name);
	void setTxtStyleName(WordStyleTxt style, const std::string& name);
	int determineListStyleNumberingId(WordStylePar from_that_style);
	
	std::vector<Field*> fieldsByName(const std::string name);
	bool fieldAssignContent(Field& field, const pugi::xml_node with_that_content, bool replace = true);
	bool fieldAssignString(Field& field, const std::string& with_this_string, bool replace = true);
	unsigned int fieldAssignContent(const std::string& this_field, const pugi::xml_node with_that_content, bool replace = true);
	unsigned int fieldAssignString(const std::string& this_field, const std::string& with_this_string, bool replace = true);
	int headerLevelAtField(const WordFile::Field& field) const;
	int headerLevelAtField(const std::string& fieldname) const;
	// TODO: assignFieldByText (formatted text)
	
	std::string insertMediaImageFile(const char* this_file_name);
};

