// kate: indent-mode cstyle; indent-width 4; replace-tabs off; tab-width 4; 

/*
* Copyright (c) Christoph Kloos 2018
* This file is subject to the terms and conditions of the
* MIT License | https://opensource.org/licenses/MIT
* SPDX-License-Identifier: MIT
*/

#pragma once

#include <pugixml.hpp>

namespace XMLHelper
{
	pugi::xml_node goUpTo(pugi::xml_node start_here, const char* to_name);
}

